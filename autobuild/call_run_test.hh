#include <Python.h>

int call_run_test(const char* test_program, const char* test_section)
{
    PyObject *pName, *pModule, *pFunc;
    PyObject *pArgs, *pValue;

    Py_Initialize();
    pName = PyString_FromString("run_test");
    /* Error checking of pName left out */
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    if (pModule != NULL) {
        pFunc = PyObject_GetAttrString(pModule, "run_test");
        /* pFunc is a new reference */

        if (pFunc && PyCallable_Check(pFunc)) {
            pArgs = PyTuple_New(2);
            pValue = PyString_FromString(test_program);
            if (!pValue) {
                Py_DECREF(pArgs);
                Py_DECREF(pModule);
                Py_DECREF(pFunc);
                fprintf(stderr, "Cannot convert argument test_program\n");
                return 1;
            }
            PyTuple_SetItem(pArgs, 0, pValue);

            pValue = PyString_FromString(test_section);
            if (!pValue) {
                Py_DECREF(pArgs);
                Py_DECREF(pModule);
                Py_DECREF(pFunc);
                fprintf(stderr, "Cannot convert argument test_section\n");
                return 1;
            }
            PyTuple_SetItem(pArgs, 1, pValue);

            pValue = PyObject_CallObject(pFunc, pArgs);
            Py_DECREF(pArgs);
            if (pValue != NULL) {
                // result of call:
                int result = PyInt_AsLong(pValue);
                Py_DECREF(pValue);
                Py_DECREF(pModule);
                Py_DECREF(pFunc);
                return result;
            }
            else {
                Py_DECREF(pModule);
                Py_DECREF(pFunc);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
                return 1;
            }
        }
        else {
            if (PyErr_Occurred())
                PyErr_Print();
            fprintf(stderr, "Cannot find function run_test\n");
        }
        Py_XDECREF(pFunc);
        Py_DECREF(pModule);
    }
    else {
        PyErr_Print();
        fprintf(stderr, "Failed to load run_test\n");
        return 1;
    }
    Py_Finalize();
    return 0;
}

