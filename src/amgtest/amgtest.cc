// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include<iostream>
#include <vector>

#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>
#include<dune/common/timer.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/common/gridfactory.hh>
#include<dune/grid/yaspgrid.hh>
#include<dune/grid/alugrid.hh>
#include<dune/grid/uggrid.hh>
#include<dune/grid/utility/structuredgridfactory.hh>
#include<dune/grid/io/file/dgfparser/dgfparser.hh>
#include<dune/grid/io/file/dgfparser/dgfug.hh>
#include<dune/grid/io/file/dgfparser/dgfyasp.hh>
#include<dune/grid/io/file/dgfparser/dgfalu.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/superlu.hh>
#include<dune/istl/paamg/amg.hh>
#include<dune/pdelab/finiteelementmap/q1fem.hh>
#include<dune/pdelab/finiteelementmap/conformingconstraints.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/constraints.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/stationary/linearproblem.hh>

#include"amg_operator.hh"
#include"novlp_parallel_amg.hh"
#include"ovlp_parallel_amg.hh"
#include "../utility/utility.hh"

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    const int par_size = helper.size();
    std::cout << "parallel run on " << par_size << " process(es)" << std::endl;
    
    Dune::GridPtr<Dune::YaspGrid<3> > gridptryaspovlp("../../src/amgtest/cube3d_ovlp.dgf");
    Dune::YaspGrid<3> & ovlp_yasp_dgf = *gridptryaspovlp;
    typedef Dune::YaspGrid<3>::LeafGridView GV_Cube_yasp;
    const GV_Cube_yasp& gv_ovlp_yasp = ovlp_yasp_dgf.leafView();

    Dune::GridPtr<Dune::YaspGrid<3> > gridptryasp("../../src/amgtest/cube3d.dgf");
    Dune::YaspGrid<3> & novlp_yasp_dgf = *gridptryasp;
    typedef Dune::YaspGrid<3>::LeafGridView GV_Cube_yasp;
    const GV_Cube_yasp& gv_novlp_yasp = novlp_yasp_dgf.leafView();
    
#if HAVE_UG
BEGIN_BLOCK(HAVE_UG)
    Dune::FieldVector<double,3> lowerLeft(0);
    Dune::FieldVector<double,3> upperRight(1);
    Dune::array<unsigned int,3> elements;
    std::fill(elements.begin(), elements.end(), 32);
    typedef Dune::UGGrid<3> GridType;
    Dune::StructuredGridFactory<GridType> structuredGridFactory;
    Dune::shared_ptr<GridType> cube_ug = 
      structuredGridFactory.createCubeGrid(lowerLeft, upperRight, elements);
    cube_ug->loadBalance();
    typedef GridType::LeafGridView GV_Cube_ug;
    const GV_Cube_ug& gv_cube_ug=cube_ug->leafView();
END_BLOCK(HAVE_UG)
#endif

#if HAVE_ALUGRID
BEGIN_BLOCK(HAVE_ALUGRID)
    Dune::GridPtr<Dune::ALUCubeGrid<3,3> > gridptralu("../../src/amgtest/cube3d.dgf");
    Dune::ALUCubeGrid<3,3> & cube_alu = *gridptralu;
    cube_alu.loadBalance();
    typedef Dune::ALUCubeGrid<3,3>::LeafGridView GV_Cube_alu;
    const GV_Cube_alu& gv_cube_alu=cube_alu.leafView();
END_BLOCK(HAVE_ALUGRID)
#endif

    if(helper.rank()==0)
      std::cout << "OVLP_BCGS_AMG_SSORk" << std::endl;
    ovlp_parallel_amg(gv_ovlp_yasp,0);
    if(helper.rank()==0)
      std::cout << std::endl;

    if(helper.rank()==0)
      std::cout << "NOVLP_BCGS_AMG_SSORk_yasp" << std::endl;
    novlp_parallel_amg(gv_novlp_yasp,0);
    if(helper.rank()==0)
      std::cout << std::endl;

#if HAVE_UG
BEGIN_BLOCK(HAVE_UG)
    if(helper.rank()==0)
      std::cout << "NOVLP_CG_AMG_SSORk_ug" << std::endl;
    novlp_parallel_amg(gv_cube_ug,1);
    if(helper.rank()==0)
      std::cout << std::endl;
END_BLOCK(HAVE_UG)
#endif

#if HAVE_ALUGRID
BEGIN_BLOCK(HAVE_ALUGRID)
    if(helper.rank()==0)
      std::cout << "NOVLP_CG_AMG_SSORk_alu" << std::endl;
    novlp_parallel_amg(gv_cube_alu,2);
    if(helper.rank()==0)
      std::cout << std::endl;
END_BLOCK(HAVE_ALUGRID)
#endif
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}

