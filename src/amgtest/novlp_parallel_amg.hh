#include "../utility/utility.hh"

template<class GV>
void novlp_parallel_amg(const GV& gv, const int i)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;
  const int dim = GV::dimension;

  // <<<2>>> Make grid function 
  typedef Dune::PDELab::Q1LocalFiniteElementMap<Coord,Real,dim> FEM;
  FEM fem;
  typedef Dune::PDELab::NonoverlappingConformingDirichletConstraints CON;
  typedef Dune::PDELab::ISTLVectorBackend<1> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  CON con;
  GFS gfs(gv,fem,con);
  con.compute_ghosts(gfs);
  typedef BCType<GV> B; // boundary condition type
  B b(gv);  
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::constraints(b,gfs,cc,false);

  // <<<3>>> Make grid operator space
  typedef AMGLocalOperator<B> LOP;
  LOP lop(b);
  typedef VBE::MatrixBackend MBE;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC,true> GO;
  GO go(gfs,cc,gfs,cc,lop);
  Dune::Timer timer;

  // <<<4>>> Make FE function with BC
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0); // initial value
  typedef BCExtension<GV,Real> G;      // boundary value + extension
  G g(gv);
  Dune::PDELab::interpolate(g,gfs,u);  // interpolate coefficient vector

  // <<<5>>> Select a linear solver backend and solve linear problem
  if(i==0){
    typedef Dune::PDELab::ISTLBackend_NOVLP_BCGS_AMG_SSOR<GO> LS;
    LS ls(gfs,5000,2);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,u,ls,1e-10);
    slp.apply();
    Dune::PDELab::LinearSolverResult<double> ls_result = ls.result();
    if (gfs.gridview().comm().rank() == 0) {
      TEST_OUTPUT("par=" << gfs.gridview().comm().size() << " IT", ls_result.iterations)
      TEST_OUTPUT("par=" << gfs.gridview().comm().size() << " rate of convergence", ls_result.conv_rate)
    }
  }

  if(i==1){
    typedef Dune::PDELab::ISTLBackend_NOVLP_CG_AMG_SSOR<GO> LS;
    LS ls(gfs,5000,2);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,u,ls,1e-10);
    slp.apply();
    Dune::PDELab::LinearSolverResult<double> ls_result = ls.result();
    if (gfs.gridview().comm().rank() == 0) {
      TEST_OUTPUT("par=" << gfs.gridview().comm().size() << " IT", ls_result.iterations)
      TEST_OUTPUT("par=" << gfs.gridview().comm().size() << " rate of convergence", ls_result.conv_rate)
    }
  }

  if(i==2){
    typedef Dune::PDELab::ISTLBackend_NOVLP_CG_AMG_SSOR<GO> LS;
    LS ls(gfs,5000,2);
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,u,ls,1e-10);
    slp.apply();
    Dune::PDELab::LinearSolverResult<double> ls_result = ls.result();
    if (gfs.gridview().comm().rank() == 0) {
      TEST_OUTPUT("par=" << gfs.gridview().comm().size() << " IT", ls_result.iterations)
      TEST_OUTPUT("par=" << gfs.gridview().comm().size() << " rate of convergence", ls_result.conv_rate)
    }
  }

  std::cout << "total calculation time=" << timer.elapsed() << std::endl;

  // <<<6>>> graphical output of ith basis function 
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
  DGF udgf(gfs,u);
  Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
  vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(udgf,"u"));
  if(i==0){
    std::stringstream fullname;
    fullname << "novlp_amg_yasp_par" << gfs.gridview().comm().size();
    vtkwriter.write(fullname.str(), Dune::VTK::ascii);
  }
  if(i==1){
    std::stringstream fullname;
    fullname << "novlp_amg_ug_par" << gfs.gridview().comm().size();
    vtkwriter.write(fullname.str(),Dune::VTK::ascii);
  }    
  if(i==2){
    std::stringstream fullname;
    fullname << "novlp_amg_alu_par" << gfs.gridview().comm().size();
    vtkwriter.write(fullname.str(),Dune::VTK::ascii);
  }    
}

