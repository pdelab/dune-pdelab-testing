// -*- tab-width: 4; indent-tabs-mode: nil -*-

/** \file

    \brief Example applications of the local operator TaylorHoodNavierStokesJacobian (stationary case).

    This file provides examples applications of Navier-Stokes flow in
    2- and 3-dimensional tubes with and without obstacles. An L-shape
    domain example is provided as well.

*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include<iostream>
#include<vector>
#include<map>
#include<string>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/float_cmp.hh>
#include<dune/common/static_assert.hh>
#include<dune/grid/yaspgrid.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/superlu.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>

#include<dune/pdelab/finiteelementmap/pk2dfem.hh>
#include<dune/pdelab/finiteelementmap/pk3dfem.hh>
#include<dune/pdelab/finiteelementmap/q12dfem.hh>
#include<dune/pdelab/finiteelementmap/q22dfem.hh>
#include<dune/pdelab/finiteelementmap/q1fem.hh>
#include<dune/pdelab/finiteelementmap/conformingconstraints.hh>
#include<dune/pdelab/finiteelementmap/hangingnodeconstraints.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/constraints.hh>
#include<dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/localoperator/laplacedirichletp12d.hh>
#include<dune/pdelab/localoperator/cg_stokes.hh>
#include <dune/common/parametertreeparser.hh>
#include<dune/pdelab/newton/newton.hh>

#include "../utility/gridexamples.hh"
#include "../utility/gridfunction.hh"
#include "../utility/utility.hh"
#include "cgstokes_initial.hh"

//! A simple parameter class which complies with the interface
//! required by the local operator.
class NavierStokesParameters
//  : public Dune::PDELab::TaylorHoodNavierStokesParameters<double>
{
public:
  double rho_;
  double mu_;
  double pressure;
  int domain_level;
  std::string example_switch_;

  double rho() const{
    return rho_;
  }

  double mu() const{
    return mu_;
  }

  const std::string example_switch() const {
    return example_switch_;
  }

  template <class ConfigParser>
  NavierStokesParameters(ConfigParser & parser)
    : rho_(1.0), mu_(1.0), pressure(1.0), domain_level(1), example_switch_("HY2")
  {
    bool valid = true;
    valid = valid & parser.hasKey("physics.mu");
    valid = valid & parser.hasKey("physics.rho");
    valid = valid & parser.hasKey("boundaries.pressure");
    valid = valid & parser.hasKey("domain.level");
    valid = valid & parser.hasKey("domain.example");

    if(!valid){
      std::cerr << "Error: The configuration file "
                << "was not valid. Default values will be used."
                << std::endl;
    }
    else{
      rho_ = parser.template get<double>("physics.rho");
      mu_ = parser.get("physics.mu",double(1.0));
      pressure = parser.get("boundaries.pressure",double(1.0));
      domain_level = parser.get("domain.level",int(1));
      example_switch_ = parser.get("domain.example","HY2");
    }
  }
};

//===============================================================
// The driver for all examples
//===============================================================
template<typename GV, typename THP, typename BF, typename NF, typename IS, typename PRM, int q>
class NavierStokesSolver
{
public:
  //! Basic grid types
  //! @{
  typedef typename GV::Grid::ctype DF;
  static const unsigned int dim = GV::dimension;
  typedef double RF;
  //! @}

  //! Common types of grid functions spaces
  //! @{
  typedef Dune::PDELab::ISTLVectorBackend<1> VectorBackend;
  typedef Dune::PDELab::ConformingDirichletConstraints Constraints;
  typedef Dune::PDELab::SimpleGridFunctionStaticSize GFSSize;
  typedef Dune::PDELab::GridFunctionSpaceLexicographicMapper GFMapper;
  //! @}

  //! The Taylor Hood pair
  typedef THP TaylorHoodPair;
  typedef typename TaylorHoodPair::V_FEM V_FEM;
  typedef typename TaylorHoodPair::P_FEM P_FEM;

  //! The scalar velocity grid function space
  typedef Dune::PDELab::GridFunctionSpace
    <GV, V_FEM, Constraints, VectorBackend, GFSSize> V_GFS;

  //! The scalar pressure grid function space
  typedef Dune::PDELab::GridFunctionSpace
    <GV, P_FEM, Constraints, VectorBackend, GFSSize> P_GFS;

  //! The vector velocity grid function space
  typedef Dune::PDELab::PowerGridFunctionSpace<V_GFS,dim,GFMapper> PGFS_V_GFS;

  //! The grid function spaces
  typedef Dune::PDELab::CompositeGridFunctionSpace<GFMapper,PGFS_V_GFS, P_GFS> GFS;

  typedef BF BoundaryFunction;
  typedef NF NeumannFlux;
  typedef IS InitialSolution;
  typedef Dune::PDELab::TaylorHoodNavierStokesJacobian<PRM,true,q> LOP;
  typedef typename GFS::template ConstraintsContainer<RF>::Type C;
  typedef VectorBackend::MatrixBackend MBE;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;

  //! The solution coefficient vector type
  typedef typename GO::Traits::Domain V;

  //! The sub grid function spaces for pressure and velocity
  //! @{
  typedef typename Dune::PDELab::GridFunctionSubSpace<GFS,0> VelocitySubGFS;
  typedef typename Dune::PDELab::GridFunctionSubSpace<GFS,1> PressureSubGFS;
  //! @}

  //! The grid functions for the pressure and velocity solution
  //! @{
  typedef Dune::PDELab::VectorDiscreteGridFunction<VelocitySubGFS,V> VelocityGridFunction;
  typedef Dune::PDELab::DiscreteGridFunction<PressureSubGFS,V> PressureGridFunction;
  //! @}

  /** \brief Constructor

      \param [in] gv_ The grid view

      \param [in] thp_ The taylor hood pair with the finite element maps
  */
  NavierStokesSolver(const GV & gv_, THP & thp_)
    : gv(gv_), thp(thp_), vFem(thp.velocityFEM()), pFem(thp.pressureFEM()),
      vGfs(gv,vFem), powerVGfs(vGfs), pGfs(gv,pFem), gfs(powerVGfs, pGfs),
      x0(gfs), velocitySubGfs(gfs), pressureSubGfs(gfs),
      vdgf(velocitySubGfs,x0), pdgf(pressureSubGfs,x0)
  {}

  template<typename IF>
  void solve(PRM & lopparameters,
             IF & initial_solution)
  {
    typedef IF InitializationFunction;

    Dune::Timer timer;
    std::cout << "=== Initialize:" << timer.elapsed() << std::endl;
    timer.reset();
    // Make constraints map and initialize it from a function
    C cg;
    cg.clear();

    // create Taylor-Hood constraints from boundary-type
    typedef Dune::PDELab::StokesVelocityDirichletConstraints<PRM>
      ScalarVelocityConstraints;
    typedef Dune::PDELab::PowerConstraintsParameters<ScalarVelocityConstraints,dim>
      VelocityConstraints;
    typedef Dune::PDELab::StokesPressureDirichletConstraints<PRM>
      PressureConstraints;
    typedef Dune::PDELab::CompositeConstraintsParameters<VelocityConstraints,PressureConstraints>
      TaylorHoodConstraints;

    ScalarVelocityConstraints scalarvelocity_constraints(lopparameters);
    VelocityConstraints velocity_constraints(scalarvelocity_constraints);
    PressureConstraints pressure_constraints(lopparameters);
    TaylorHoodConstraints th_constraints(velocity_constraints,pressure_constraints);

    Dune::PDELab::constraints(th_constraints,gfs,cg);

    // Make coefficent vector and initialize it from a function
    x0 = 0.0;
    Dune::PDELab::NavierStokesDirichletFunctionAdapterFactory<PRM> dirichletFunctionFactory(lopparameters);
    Dune::PDELab::interpolate(dirichletFunctionFactory.dirichletFunction(),gfs,x0);

    std::cout << "=== Finished interpolation:" << timer.elapsed() << std::endl;
    timer.reset();

    // Set non constrained dofs to zero
    Dune::PDELab::set_shifted_dofs(cg,0.0,x0);

    // Make grid function operator
    LOP lop(lopparameters);
    GO go(gfs,cg,gfs,cg,lop);

    //Dune::printmatrix(std::cout,m.base(),"global stiffness matrix","row",9,1);

    // Linear solver
    typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LinearSolver;
    LinearSolver ls(false);

    // Solve (possibly) nonlinear problem
    std::cout << "=== Begin Newton:" << std::endl;
    timer.reset();
    Dune::PDELab::Newton<GO,LinearSolver,V> newton(go,x0,ls);
    newton.setReassembleThreshold(0.0);
    newton.setVerbosityLevel(2);
    newton.setMaxIterations(25);
    newton.setLineSearchMaxIterations(30);
    newton.apply();
    std::cout << "=== Finished Newton:" << timer.elapsed() << std::endl;

    // Check residual
    V r(gfs); r=0.;
    go.residual(x0,r);
    std::cout << "Final Residual: " << r.two_norm() << std::endl;
  }

  void writeVTKFile(std::string filename)
  {
    // Output grid function with SubsamplingVTKWriter
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,2);
    vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<PressureGridFunction>(pdgf,"p"));
    vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<VelocityGridFunction>(vdgf,"v"));
    vtkwriter.write(filename,Dune::VTK::ascii);
  }

  const VelocityGridFunction & velocityGridFunction(){
    return vdgf;
  }

  const PressureGridFunction & pressureGridFunction(){
    return pdgf;
  }

private:
  const GV gv;
  THP thp;
  const V_FEM & vFem;
  const P_FEM & pFem;
  V_GFS vGfs;
  PGFS_V_GFS powerVGfs;
  P_GFS pGfs;
  GFS gfs;
  V x0;
  VelocitySubGFS velocitySubGfs;
  PressureSubGFS pressureSubGfs;
  VelocityGridFunction vdgf;
  PressureGridFunction pdgf;
};


template<typename GV, typename DF, typename R, int k>
class Pk2DTaylorHoodPair{
public:
  typedef Dune::PDELab::Pk2DLocalFiniteElementMap<GV,DF,R,k> V_FEM;
  typedef Dune::PDELab::Pk2DLocalFiniteElementMap<GV,DF,R,k-1> P_FEM;

  Pk2DTaylorHoodPair(const GV & gv_) : gv(gv_), vFem(gv), pFem(gv)
  {}

  Pk2DTaylorHoodPair(const Pk2DTaylorHoodPair & c)
    : gv(c.gv), vFem(gv), pFem(gv)
  {}

  const V_FEM & velocityFEM() { return vFem; }
  const P_FEM & pressureFEM() { return pFem; }

private:
  GV gv;
  V_FEM vFem;
  P_FEM pFem;
};

template<typename GV, typename DF, typename R, int k>
class Pk3DTaylorHoodPair{
public:
  typedef Dune::PDELab::Pk3DLocalFiniteElementMap<GV,DF,R,k> V_FEM;
  typedef Dune::PDELab::Pk3DLocalFiniteElementMap<GV,DF,R,k-1> P_FEM;

  Pk3DTaylorHoodPair(const GV & gv_) : gv(gv_), vFem(gv), pFem(gv)
  {}

  Pk3DTaylorHoodPair(const Pk3DTaylorHoodPair & c)
    : gv(c.gv), vFem(gv), pFem(gv)
  {}

  const V_FEM & velocityFEM() { return vFem; }
  const P_FEM & pressureFEM() { return pFem; }

private:
  GV gv;
  V_FEM vFem;
  P_FEM pFem;
};

template <typename DF, typename R>
class Q22DTaylorHoodPair{
public:
  typedef Dune::PDELab::Q22DLocalFiniteElementMap<DF,R> V_FEM;
  typedef Dune::PDELab::Q1LocalFiniteElementMap<DF,R,2> P_FEM;

  Q22DTaylorHoodPair() : vFem(), pFem()
  {}

  Q22DTaylorHoodPair(const Q22DTaylorHoodPair & c)
    : vFem(), pFem()
  {}

  const V_FEM & velocityFEM() { return vFem; }
  const P_FEM & pressureFEM() { return pFem; }

private:
  V_FEM vFem;
  P_FEM pFem;
};

template<typename Solver, typename Errors>
void computeL2Error(Solver & reference, Solver & test, Errors & l2errors,
                    const int func_order, const int level){
  // Compute l2 error
  typedef MultiLevelDifferenceSquaredAdapter
    < typename Solver::VelocityGridFunction, typename Solver::VelocityGridFunction >
    ErrorGridFunction;
  ErrorGridFunction error_gf(reference.velocityGridFunction(),test.velocityGridFunction());
  typename ErrorGridFunction::Traits::RangeType l2error(0);
  Dune::PDELab::integrateGridFunction(error_gf,l2error,func_order*4);
//  std::cout << "!T! L2-Error on level " << level << " : " << std::scientific << l2error << std::endl;
  TEST_OUTPUT("L2-Error on level " << level, std::scientific << l2error)
  l2errors[level]=l2error;
}

template<typename Solver>
void computeL2Norm(Solver & reference, const int func_order, const int level){
  // Compute l2 error
  typedef SquaredAdapter<typename Solver::VelocityGridFunction>
    L2NormGridFunction;
  L2NormGridFunction norm_gf(reference.velocityGridFunction());
  typename L2NormGridFunction::Traits::RangeType l2norm(0);
  Dune::PDELab::integrateGridFunction(norm_gf,l2norm,func_order*4);
//  std::cout << "!T! L2-Norm on level " << level << " : " << std::scientific << l2norm << std::endl;
  TEST_OUTPUT("L2-Norm on level " << level, std::scientific << l2norm)
}

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
#if !HAVE_SUPERLU
  std::cerr << "Error: These examples work only if SuperLU is available." << std::endl;
  exit(1);
#endif

    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
      {
        if(helper.rank()==0)
          std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
      }

      if (argc!=2)
        {
          if(helper.rank()==0)
            std::cout << "usage: ./executable <config file>" << std::endl;
          return 1;
        }

    // Parse configuration file.
    std::string config_file(argv[1]);
    Dune::ParameterTree config_parameters;
    try{
      Dune::ParameterTreeParser::readINITree(config_file, config_parameters);
    }
    catch(...){
      std::cerr << "Could not read config file \""
                << config_file << "\"!" << std::endl;
      exit(1);
    }
    NavierStokesParameters parameters(config_parameters);
  try{
    // Yasp Grid Hagen-Poiseuille test 2D
    if(parameters.example_switch().find("HY2") != std::string::npos) {
      typedef double RF;
      // make grid
      Dune::FieldVector<double,2> L(1.0);
      Dune::FieldVector<int,2> N(1);
      Dune::FieldVector<bool,2> B(false);
      Dune::YaspGrid<2> grid(L,N,B,0);
      grid.globalRefine(parameters.domain_level);

      const int p=2;
      const int q=2*p;

      typedef Q22DTaylorHoodPair<RF,RF> TaylorHoodPair;
      TaylorHoodPair thp;

      typedef Dune::YaspGrid<2>::LevelGridView GV;

      typedef BCTypeParam_HagenPoiseuille BoundaryFunction;
      typedef HagenPoiseuilleZeroFlux<GV,RF> NeumannFlux;

      typedef HagenPoiseuilleVelocity<GV,RF,2> InitialVelocity;
      typedef ZeroScalarFunction<GV,RF> InitialPressure;
      typedef Dune::PDELab::CompositeGridFunction<InitialVelocity,InitialPressure> InitialSolution;
      typedef Dune::PDELab::TaylorHoodNavierStokesDefaultParameters
        <GV,BoundaryFunction,NeumannFlux,InitialSolution,RF>
        LOPParameters;

      typedef NavierStokesSolver<GV,TaylorHoodPair,BoundaryFunction,NeumannFlux,InitialSolution,LOPParameters,q> Solver;
      std::vector< Dune::shared_ptr<Solver> > solvers;
      std::vector<RF> l2errors(parameters.domain_level);

      const std::string filename_base = "hagenpoiseuille_yasp_Q2Q1_2d";
      for(int level = parameters.domain_level; level >= 0 ; --level){
        // get view
        const GV& gv=grid.levelView(level);

        InitialVelocity init_velocity(gv);
        InitialPressure init_pressure(gv);
        InitialSolution initial_solution(init_velocity,init_pressure);

        BoundaryFunction boundary_function;
        NeumannFlux neumann_flux(gv);
        LOPParameters lopparameters(config_parameters.sub("physics"),boundary_function,neumann_flux, initial_solution);

        // solve problem
        solvers.push_back(Dune::shared_ptr<Solver>(new Solver(gv,thp)));
        Solver & solver = *(solvers.back());
        solver.solve<InitialSolution>
          (lopparameters,initial_solution);

        // write vtk file
        std::stringstream vtkfilename;
        vtkfilename << filename_base << "_level_" << level;
        solver.writeVTKFile(vtkfilename.str());

        // compute l2 error
        if(solvers.size() > 1)
          computeL2Error(*(solvers.front()),*(solvers.back()),l2errors,p,level);
        computeL2Norm(*(solvers.back()),p,level);

      } // level
    }

#if HAVE_ALUGRID
BEGIN_BLOCK(HAVE_ALUGRID)
    // ALU Grid Hagen-Poiseuille test 2D
    if(parameters.example_switch().find("HA2") != std::string::npos)
    {
      // make grid
      typedef ALUUnitCube<2> UnitCube;
      UnitCube unitcube;
      unitcube.grid().globalRefine(parameters.domain_level);

      // get view
      typedef UnitCube::GridType::LevelGridView GV;

      // make finite element map
      typedef UnitCube::GridType::ctype DF;
      typedef double RF;
      const int k=2;
      const int q=2*k;
      typedef Pk2DTaylorHoodPair<GV,DF,RF,k> TaylorHoodPair;

      typedef BCTypeParam_HagenPoiseuille BoundaryFunction;
      typedef HagenPoiseuilleZeroFlux<GV,RF> NeumannFlux;

      typedef HagenPoiseuilleVelocity<GV,RF,2> InitialVelocity;
      typedef ZeroScalarFunction<GV,RF> InitialPressure;
      typedef Dune::PDELab::CompositeGridFunction<InitialVelocity,InitialPressure> InitialSolution;
      typedef Dune::PDELab::TaylorHoodNavierStokesDefaultParameters
        <GV,BoundaryFunction,NeumannFlux,InitialSolution,RF>
        LOPParameters;

      typedef NavierStokesSolver<GV,TaylorHoodPair,BoundaryFunction,NeumannFlux,InitialSolution,LOPParameters,q> Solver;
      std::vector< Dune::shared_ptr<Solver> > solvers;
      std::vector<RF> l2errors(parameters.domain_level);

      const std::string filename_base = "hagenpoiseuille_alu_P2P1_2d";

      for(int level = parameters.domain_level; level >= 0 ; --level){

        const GV& gv=unitcube.grid().levelView(level);

        TaylorHoodPair thp(gv);

        InitialVelocity init_velocity(gv);
        InitialPressure init_pressure(gv);
        InitialSolution initial_solution(init_velocity,init_pressure);

        BoundaryFunction boundary_function;
        NeumannFlux neumann_flux(gv);

        LOPParameters lopparameters(config_parameters.sub("physics"),boundary_function,neumann_flux,initial_solution);

        // solve problem
        solvers.push_back(Dune::shared_ptr<Solver>(new Solver(gv,thp)));
        Solver & solver = *(solvers.back());
        solver.solve<InitialSolution>
          (lopparameters,initial_solution);

        // write vtk file
        std::stringstream vtkfilename;
        vtkfilename << filename_base << "_level_" << level;
        solver.writeVTKFile(vtkfilename.str());

        // compute l2 error
        if(solvers.size() > 1)
          computeL2Error(*(solvers.front()),*(solvers.back()),l2errors,k,level);
        computeL2Norm(*(solvers.back()),k,level);
      }

    }
END_BLOCK(HAVE_ALUGRID)
#endif

#if HAVE_UG
BEGIN_BLOCK(HAVE_UG)
    // UG Grid turbulence tube test 2D
    if(parameters.example_switch().find("TU2") != std::string::npos)
    {
      typedef Dune::UGGrid<2> GridType;
      Dune::GridFactory<GridType> grid_factory;

      typedef double RF;

      std::vector<int> boundary_index_map;
      std::vector<int> element_index_map;

      std::string grid_file = "../../src/utility/grids/turbtube2d.msh";
      Dune::GmshReader<GridType> gmsh_reader;
      gmsh_reader.read(grid_factory,grid_file,boundary_index_map,element_index_map,true,false);
      Dune::shared_ptr<GridType> grid_pointer( grid_factory.createGrid() );
      GridType & grid = *grid_pointer;
      grid.globalRefine(parameters.domain_level);

      // get view
      typedef GridType::LevelGridView GV;

      // make finite element map
      typedef GridType::ctype DF;
      typedef double R;
      const int k=2;
      const int q=2*k;
      typedef Pk2DTaylorHoodPair<GV,DF,RF,k> TaylorHoodPair;

      typedef BCTypeParam_PressureDrop<GV> BoundaryFunction;
      typedef PressureDropFlux<GV,RF> NeumannFlux;

      typedef ZeroScalarFunction<GV,RF> ZeroFunction;
      typedef Dune::PDELab::PowerGridFunction<ZeroFunction,2> InitialVelocity;
      typedef ZeroFunction InitialPressure;
      typedef Dune::PDELab::CompositeGridFunction<InitialVelocity,InitialPressure> InitialSolution;
      typedef Dune::PDELab::TaylorHoodNavierStokesDefaultParameters
        <GV,BoundaryFunction,NeumannFlux,InitialSolution,RF>
        LOPParameters;

      typedef NavierStokesSolver<GV,TaylorHoodPair,BoundaryFunction,NeumannFlux,InitialSolution,LOPParameters,q> Solver;
      std::vector< Dune::shared_ptr<Solver> > solvers;
      std::vector<RF> l2errors(parameters.domain_level);

      const std::string filename_base = "turbtube_ug_P2P1_2d";

      for(int level = parameters.domain_level; level >= 0 ; --level){

        const GV& gv=grid.levelView(level);

        TaylorHoodPair thp(gv);

        ZeroFunction zero_function(gv);
        InitialVelocity init_velocity(zero_function);
        InitialPressure init_pressure(gv);
        InitialSolution initial_solution(init_velocity,init_pressure);

        // Domain parameters:
        const int tube_direction = 0; // Tube in x-axes direction
        const RF tube_length = 5.0;
        const RF tube_origin = 0.0;

        BoundaryFunction boundary_function(tube_length, tube_origin, tube_direction);
        NeumannFlux neumann_flux(gv, parameters.pressure, tube_length, tube_origin, tube_direction);

        LOPParameters lopparameters(config_parameters.sub("physics"),boundary_function,neumann_flux,initial_solution);

        // solve problem
        solvers.push_back(Dune::shared_ptr<Solver>(new Solver(gv,thp)));
        Solver & solver = *(solvers.back());
        solver.solve<InitialSolution>
          (lopparameters,initial_solution);

        // write vtk file
        std::stringstream vtkfilename;
        vtkfilename << filename_base << "_level_" << level;
        solver.writeVTKFile(vtkfilename.str());

        // compute l2 error
        if(solvers.size() > 1)
          computeL2Error(*(solvers.front()),*(solvers.back()),l2errors,k,level);
        computeL2Norm(*(solvers.back()),k,level);
      }

      // Compute rates of convergence
      for(unsigned int level = 0; level + 1 < l2errors.size(); ++level){
        const RF rate = std::log(l2errors[level]/l2errors[level+1])/std::log(2.0);
//        std::cout << "!T! Convergence from level " << level << " to " << level + 1 << " : " <<
//          rate << std::endl;
          TEST_OUTPUT("Convergence from level " << level << " to " << level + 1, rate)
      }

    }
END_BLOCK(HAVE_UG)
#endif

#if HAVE_UG
BEGIN_BLOCK(HAVE_UG)
    // UG Grid L-shape domain test 2D
    if(parameters.example_switch().find("LU2") != std::string::npos)
    {
      typedef Dune::UGGrid<2> GridType;
      Dune::GridFactory<GridType> grid_factory;

      typedef double RF;

      std::vector<int> boundary_index_map;
      std::vector<int> element_index_map;

      std::string grid_file = "../../src/utility/grids/lshape.msh";
      Dune::GmshReader<GridType> gmsh_reader;
      gmsh_reader.read(grid_factory,grid_file,boundary_index_map,element_index_map,true,false);
      Dune::shared_ptr<GridType> grid_pointer( grid_factory.createGrid() );
      GridType & grid = *grid_pointer;
      grid.globalRefine(parameters.domain_level);

      // get view
      typedef GridType::LevelGridView GV;

      // make finite element map
      typedef GridType::ctype DF;
      typedef double R;
      const int k=2;
      const int q=2*k;
      typedef Pk2DTaylorHoodPair<GV,DF,RF,k> TaylorHoodPair;

      typedef BCTypeParam_PressureDrop<GV> BoundaryFunction;
      typedef PressureDropFlux<GV,RF> NeumannFlux;

      typedef ZeroScalarFunction<GV,RF> ZeroFunction;
      typedef Dune::PDELab::PowerGridFunction<ZeroFunction,2> InitialVelocity;
      typedef ZeroFunction InitialPressure;
      typedef Dune::PDELab::CompositeGridFunction<InitialVelocity,InitialPressure> InitialSolution;
      typedef Dune::PDELab::TaylorHoodNavierStokesDefaultParameters
        <GV,BoundaryFunction,NeumannFlux,InitialSolution,RF>
        LOPParameters;

      typedef NavierStokesSolver<GV,TaylorHoodPair,BoundaryFunction,NeumannFlux,InitialSolution,LOPParameters,q> Solver;
      std::vector< Dune::shared_ptr<Solver> > solvers;
      std::vector<RF> l2errors(parameters.domain_level);

      const std::string filename_base = "lshape_ug_P2P1_2d";

      for(int level = parameters.domain_level; level >= 0 ; --level){

        const GV& gv=grid.levelView(level);

        TaylorHoodPair thp(gv);

        ZeroFunction zero_function(gv);
        InitialVelocity init_velocity(zero_function);
        InitialPressure init_pressure(gv);
        InitialSolution initial_solution(init_velocity,init_pressure);

        // Domain parameters:
        const int tube_direction = 0; // Tube in x-axes direction
        const RF tube_length = 6.0;
        const RF tube_origin = -1.0;

        BoundaryFunction boundary_function(tube_length, tube_origin, tube_direction);
        NeumannFlux neumann_flux(gv, parameters.pressure, tube_length, tube_origin, tube_direction);

        LOPParameters lopparameters(config_parameters.sub("physics"),boundary_function,neumann_flux,initial_solution);

        // solve problem
        solvers.push_back(Dune::shared_ptr<Solver>(new Solver(gv,thp)));
        Solver & solver = *(solvers.back());
        solver.solve<InitialSolution>
          (lopparameters,initial_solution);

        // write vtk file
        std::stringstream vtkfilename;
        vtkfilename << filename_base << "_level_" << level;
        solver.writeVTKFile(vtkfilename.str());

        // compute l2 error
        if(solvers.size() > 1)
          computeL2Error(*(solvers.front()),*(solvers.back()),l2errors,k,level);
        computeL2Norm(*(solvers.back()),k,level);
      }

      // Compute rates of convergence
      for(unsigned int level = 0; level + 1 < l2errors.size(); ++level){
        const RF rate = std::log(l2errors[level]/l2errors[level+1])/std::log(2.0);
//        std::cout << "!T! Convergence from level " << level << " to " << level + 1 << " : " <<
//          rate << std::endl;
          TEST_OUTPUT("Convergence from level " << level << " to " << level + 1, rate)
      }

    }
END_BLOCK(HAVE_UG)
#endif

#if HAVE_UG
BEGIN_BLOCK(HAVE_UG)
    // UG Grid Hagen-Poiseuille test 3D
    if(parameters.example_switch().find("HU3") != std::string::npos)
    {
      typedef Dune::UGGrid<3> GridType;
      Dune::GridFactory<GridType> grid_factory;

      typedef double RF;

      std::vector<int> boundary_index_map;
      std::vector<int> element_index_map;

      std::string grid_file = "../../src/utility/grids/pipe.msh";
      Dune::GmshReader<GridType> gmsh_reader;
      gmsh_reader.read(grid_factory,grid_file,boundary_index_map,element_index_map,true,false);
      Dune::shared_ptr<GridType> grid_pointer( grid_factory.createGrid() );
      GridType & grid = *grid_pointer;
      grid.globalRefine(parameters.domain_level);

      // get view
      typedef GridType::LevelGridView GV;

      // make finite element map
      typedef GridType::ctype DF;
      typedef double R;
      const int k=2;
      const int q=2*k;
      typedef Pk3DTaylorHoodPair<GV,DF,RF,k> TaylorHoodPair;

      typedef BCTypeParam_HagenPoiseuille BoundaryFunction;
      typedef HagenPoiseuilleZeroFlux<GV,RF> NeumannFlux;

      typedef HagenPoiseuilleVelocity<GV,RF,3> InitialVelocity;
      typedef ZeroScalarFunction<GV,RF> InitialPressure;
      typedef Dune::PDELab::CompositeGridFunction<InitialVelocity,InitialPressure> InitialSolution;
      typedef Dune::PDELab::TaylorHoodNavierStokesDefaultParameters
        <GV,BoundaryFunction,NeumannFlux,InitialSolution,RF>
        LOPParameters;

      typedef NavierStokesSolver<GV,TaylorHoodPair,BoundaryFunction,NeumannFlux,InitialSolution,LOPParameters,q> Solver;
      std::vector< Dune::shared_ptr<Solver> > solvers;
      std::vector<RF> l2errors(parameters.domain_level);

      const std::string filename_base = "hagenpoiseuille_ug_P2P1_3d";

      for(int level = parameters.domain_level; level >= 0 ; --level){

        const GV& gv=grid.levelView(level);

        TaylorHoodPair thp(gv);

        InitialVelocity init_velocity(gv);
        InitialPressure init_pressure(gv);
        InitialSolution initial_solution(init_velocity,init_pressure);

        BoundaryFunction boundary_function;
        NeumannFlux neumann_flux(gv);

        LOPParameters lopparameters(config_parameters.sub("physics"),boundary_function,neumann_flux,initial_solution);

        // solve problem
        solvers.push_back(Dune::shared_ptr<Solver>(new Solver(gv,thp)));
        Solver & solver = *(solvers.back());
        solver.solve<InitialSolution>
          (lopparameters,initial_solution);

        // write vtk file
        std::stringstream vtkfilename;
        vtkfilename << filename_base << "_level_" << level;
        solver.writeVTKFile(vtkfilename.str());

        // compute l2 error
        if(solvers.size() > 1)
          computeL2Error(*(solvers.front()),*(solvers.back()),l2errors,k,level);
        computeL2Norm(*(solvers.back()),k,level);
      }

    }
END_BLOCK(HAVE_UG)
#endif

#if HAVE_UG
BEGIN_BLOCK(HAVE_UG)
    // UG Grid turbulence tube test 3D
    if(parameters.example_switch().find("TU3") != std::string::npos)
    {
      typedef Dune::UGGrid<3> GridType;
      Dune::GridFactory<GridType> grid_factory;

      std::vector<int> boundary_index_map;
      std::vector<int> element_index_map;

      std::string grid_file = "../../src/utility/grids/turbtube.msh";
      Dune::GmshReader<GridType> gmsh_reader;
      gmsh_reader.read(grid_factory,grid_file,boundary_index_map,element_index_map,true,false);
      Dune::shared_ptr<GridType> grid_pointer( grid_factory.createGrid() );
      GridType & grid = *grid_pointer;
      grid.globalRefine(parameters.domain_level);

      // get view
      typedef GridType::LevelGridView GV;

      typedef double RF;

      // make finite element map
      typedef GridType::ctype DF;
      typedef double R;
      const int k=2;
      const int q=2*k;
      typedef Pk3DTaylorHoodPair<GV,DF,RF,k> TaylorHoodPair;

      typedef BCTypeParam_PressureDrop<GV> BoundaryFunction;
      typedef PressureDropFlux<GV,RF> NeumannFlux;

      typedef ZeroScalarFunction<GV,RF> ZeroFunction;
      typedef Dune::PDELab::PowerGridFunction<ZeroFunction,3> InitialVelocity;
      typedef ZeroFunction InitialPressure;
      typedef Dune::PDELab::CompositeGridFunction<InitialVelocity,InitialPressure> InitialSolution;
      typedef Dune::PDELab::TaylorHoodNavierStokesDefaultParameters
        <GV,BoundaryFunction,NeumannFlux,InitialSolution,RF>
        LOPParameters;

      typedef NavierStokesSolver<GV,TaylorHoodPair,BoundaryFunction,NeumannFlux,InitialSolution,LOPParameters,q> Solver;
      std::vector< Dune::shared_ptr<Solver> > solvers;
      std::vector<RF> l2errors(parameters.domain_level);

      const std::string filename_base = "turbtube_ug_P2P1_3d";

      for(int level = parameters.domain_level; level >= 0 ; --level){

        const GV& gv=grid.levelView(level);

        TaylorHoodPair thp(gv);

        ZeroFunction zero_function(gv);
        InitialVelocity init_velocity(zero_function);
        InitialPressure init_pressure(gv);
        InitialSolution initial_solution(init_velocity,init_pressure);

        // Domain parameters:
        const int tube_direction = 2; // Tube in z-axes direction
        const RF tube_length = 5.0;
        const RF tube_origin = 0.0;

        BoundaryFunction boundary_function(tube_length, tube_origin, tube_direction);
        NeumannFlux neumann_flux(gv, parameters.pressure, tube_length, tube_origin, tube_direction);

        LOPParameters lopparameters(config_parameters.sub("physics"),boundary_function,neumann_flux,initial_solution);

        // solve problem
        solvers.push_back(Dune::shared_ptr<Solver>(new Solver(gv,thp)));
        Solver & solver = *(solvers.back());
        solver.solve<InitialSolution>
          (lopparameters,initial_solution);

        // write vtk file
        std::stringstream vtkfilename;
        vtkfilename << filename_base << "_level_" << level;
        solver.writeVTKFile(vtkfilename.str());

        // compute l2 error
        if(solvers.size() > 1)
          computeL2Error(*(solvers.front()),*(solvers.back()),l2errors,k,level);
        computeL2Norm(*(solvers.back()),k,level);
      }

      // Compute rates of convergence
      for(unsigned int level = 0; level + 1 < l2errors.size(); ++level){
        const RF rate = std::log(l2errors[level]/l2errors[level+1])/std::log(2.0);
//        std::cout << "!T! Convergence from level " << level << " to " << level + 1 << " : " <<
//          rate << std::endl;
        TEST_OUTPUT("Convergence from level " << level << " to " << level + 1, rate)
      }
    }
END_BLOCK(HAVE_UG)
#endif

    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
