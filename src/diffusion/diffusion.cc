// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file
    \brief High-level test with Poisson equation
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include<iostream>
#include<vector>
#include<map>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>
#include<dune/common/timer.hh>
#include<dune/grid/yaspgrid.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/paamg/amg.hh>
#include<dune/istl/superlu.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include<dune/pdelab/finiteelementmap/monomfem.hh>
#include<dune/pdelab/finiteelementmap/opbfem.hh>
#include<dune/pdelab/finiteelementmap/qkdg.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/localoperator/diffusiondg.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include<dune/pdelab/localoperator/convectiondiffusiondg.hh>
#include<dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include<dune/pdelab/stationary/linearproblem.hh>

#include<dune/pdelab/newton/newton.hh>  // used here only for Dune::PDELab::LinearSolverResult

#include"../utility/gridexamples.hh"
#include"../utility/utility.hh"
#include"src/Features.h"

const bool graphics = true;

template<typename GV, typename RF>
class Parameter
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1 : 0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    typename Traits::RangeFieldType norm = xglobal.two_norm2();
    return (2.0*GV::dimension-4.0*norm)*exp(-norm);
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    typename Traits::RangeFieldType norm = xglobal.two_norm2();
    return exp(-norm);
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }
};

/*! \brief Adapter returning ||f1(x)-f2(x)||^2 for two given grid functions

  \tparam T1  a grid function type
  \tparam T2  a grid function type
*/
template<typename T1, typename T2>
class DifferenceSquaredAdapter
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
				   typename T1::Traits::RangeFieldType,
				   1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> >
  ,DifferenceSquaredAdapter<T1,T2> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                           typename T1::Traits::RangeFieldType,
                                           1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> > Traits;

  //! constructor
  DifferenceSquaredAdapter (const T1& t1_, const T2& t2_) : t1(t1_), t2(t2_) {}

  //! \copydoc GridFunctionBase::evaluate()
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    typename T1::Traits::RangeType y1;
    t1.evaluate(e,x,y1);
    typename T2::Traits::RangeType y2;
    t2.evaluate(e,x,y2);
    y1 -= y2;
    y = y1.two_norm2();
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return t1.getGridView();
  }

private:
  const T1& t1;
  const T2& t2;
};

//! solve problem with DG method
template<class GV, class FEM, class PROBLEM, int degree, int blocksize>
void runDG (const GV& gv, const FEM& fem, PROBLEM& problem,
            std::string basename, int level, std::string method, std::string weights, double alpha)
{
  // coordinate and result type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;
  const int dim = GV::Grid::dimension;
  std::stringstream fullname;
  fullname << basename << "_" << method << "_w" << weights << "_k" << degree << "_dim" << dim << "_level" << level;

  // make grid function space
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTLVectorBackend<Dune::PDELab::ISTLParameters::static_blocking,blocksize> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);

  // make local operator
  Dune::PDELab::ConvectionDiffusionDGMethod::Type m;
  if (method=="SIPG") m = Dune::PDELab::ConvectionDiffusionDGMethod::SIPG;
  if (method=="NIPG") m = Dune::PDELab::ConvectionDiffusionDGMethod::NIPG;
  Dune::PDELab::ConvectionDiffusionDGWeights::Type w;
  if (weights=="ON") w = Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn;
  if (weights=="OFF") w = Dune::PDELab::ConvectionDiffusionDGWeights::weightsOff;
  typedef Dune::PDELab::ConvectionDiffusionDG<PROBLEM,FEM> LOP;
  LOP lop(problem,m,w,alpha);
  typedef Dune::PDELab::ISTLMatrixBackend MBE;
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop);

  // make a vector of degree of freedom vectors and initialize it with Dirichlet extension
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0);
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<PROBLEM> G;
  G g(gv,problem);

  // make linear solver and solve problem
  if (method=="SIPG")
    {
      typedef Dune::PDELab::ISTLBackend_SEQ_CG_ILU0 LS;
      LS ls(10000,1);
      typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
      SLP slp(go,u,ls,1e-12);
      slp.apply();
      Dune::PDELab::LinearSolverResult<double> ls_result = ls.result();
      TEST_OUTPUT("dG-Level=" << level << " IT", ls_result.iterations)
      TEST_OUTPUT("dG-Level=" << level << " rate of convergence", ls_result.conv_rate)
    }
  else
    {
      typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_ILU0 LS;
      LS ls(10000,1);
      typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
      SLP slp(go,u,ls,1e-12);
      slp.apply();
      Dune::PDELab::LinearSolverResult<double> ls_result ( ls.result() );
      TEST_OUTPUT("dG-Level=" << level << " IT", ls_result.iterations)
      TEST_OUTPUT("dG-Level=" << level << " rate of convergence", ls_result.conv_rate)
    }

  // compute L2 error
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> UDGF;
  UDGF udgf(gfs,u);
  typedef DifferenceSquaredAdapter<G,UDGF> DifferenceSquared;
  DifferenceSquared differencesquared(g,udgf);
  typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
  Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,12);

  TEST_OUTPUT("dG-Level=" << level << " gfs-globalsize", gfs.globalSize())
  TEST_OUTPUT("dG-Level=" << level << " L2ERROR", std::setw(11) << std::setprecision(7) << std::scientific << std::uppercase << sqrt(l2errorsquared[0]))

  // write vtk file
  if (graphics)
    {
      Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,degree-1);
      vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<UDGF>(udgf,"u_h"));
      vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<G>(g,"u"));
      vtkwriter.write(fullname.str(),Dune::VTK::ascii);
    }
  std::cout << "==========================================" << std::endl;
}


//! solve problem with DG method
template<class GV, class FEM, class PROBLEM, int degree>
void runFEM (const GV& gv, const FEM& fem, PROBLEM& problem, std::string basename, int level)
{
  // coordinate and result type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;
  const int dim = GV::Grid::dimension;
  std::stringstream fullname;
  fullname << basename << "_FEM" << "_k" << degree << "_dim" << dim << "_level" << level;

  // make grid function space
  typedef Dune::PDELab::ISTLVectorBackend<> VBE;
  typedef Dune::PDELab::ConformingDirichletConstraints CON;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);

  // make constraints container and initialize it
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;

  // make local operator
  typedef Dune::PDELab::ConvectionDiffusionFEM<PROBLEM,FEM> LOP;
  LOP lop(problem);
  typedef Dune::PDELab::ISTLMatrixBackend MBE;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop);

  // make a vector of degree of freedom vectors and initialize it with Dirichlet extension
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0);
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<PROBLEM> G;
  G g(gv,problem);
  Dune::PDELab::interpolate(g,gfs,u);

  Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<PROBLEM> bctype(gv,problem);
  Dune::PDELab::constraints(bctype,gfs,cc);
  Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);

  // make linear solver and solve problem
  typedef Dune::PDELab::ISTLBackend_SEQ_CG_ILU0 LS;
  LS ls(10000,1);
  typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  SLP slp(go,u,ls,1e-12);
  slp.apply();
  Dune::PDELab::LinearSolverResult<double> ls_result ( ls.result() );
  TEST_OUTPUT("FEM-Level=" << level << " IT", ls_result.iterations)
  TEST_OUTPUT("FEM-Level=" << level << " rate of convergence", ls_result.conv_rate)

  // compute L2 error
  typedef Dune::PDELab::DiscreteGridFunction<GFS,U> UDGF;
  UDGF udgf(gfs,u);
  typedef DifferenceSquaredAdapter<G,UDGF> DifferenceSquared;
  DifferenceSquared differencesquared(g,udgf);
  typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
  Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,12);

  /*
  std::cout << fullname.str()
            << " N=" << std::setw(11) << gfs.globalSize()
            << " L2ERROR=" << std::setw(11) << std::setprecision(3) << std::scientific << std::uppercase << sqrt(l2errorsquared[0]) << std::endl;
  */

  TEST_OUTPUT("FEM-Level=" << level << " gfs-globalsize", gfs.globalSize())
  TEST_OUTPUT("FEM-Level=" << level << " L2ERROR", std::setw(11) << std::setprecision(7) << std::scientific << std::uppercase << sqrt(l2errorsquared[0]))

  // write vtk file
  if (graphics)
    {
      Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,degree-1);
      vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<UDGF>(udgf,"u_h"));
      vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<G>(g,"u"));
      vtkwriter.write(fullname.str(),Dune::VTK::ascii);
    }
}

int main(int argc, char** argv)
{
  //Maybe initialize Mpi
  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
  if(Dune::MPIHelper::isFake)
    std::cout<< "This is a sequential program." << std::endl;
  else
    {
      if(helper.rank()==0)
        std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
    }

  try
    {
      Features features;
      if (features.mesh()=="CUBE")
        {
          const int dim = Features::F_DIM;
          Dune::FieldVector<double,dim> L(1.0);
          Dune::FieldVector<int,dim> N(1);
          Dune::FieldVector<bool,dim> P(false);
          typedef Dune::YaspGrid<dim> Grid;
          Grid grid(L,N,P,0);
          typedef Grid::LeafGridView GV;
          for (int i=0; i<=features.maxlevel(); ++i)
            {
              const GV& gv=grid.leafView();
              typedef Parameter<GV,double> PROBLEM;
              PROBLEM problem;
              const int degree=Features::F_DEGREE;

              if (features.method()=="SIPG") {
                  typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
                  FEMDG femdg;
                  const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
                  runDG<GV,FEMDG,PROBLEM,degree,blocksize>(gv,femdg,problem,"CUBE",i,"SIPG","ON",2.0);
              }
              if (features.method()=="FEM") {
                  typedef Dune::PDELab::QkCGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMCG;
                  FEMCG femcg;
                  runFEM<GV,FEMCG,PROBLEM,degree>(gv,femcg,problem,"CUBE",i);
              }
              // refine grid
              if (i<features.maxlevel()) grid.globalRefine(1);
            }
        }
#if HAVE_ALUGRID
BEGIN_BLOCK(HAVE_ALUGRID)
      if (features.mesh()=="SIMPLEX")
        {
          const int dim    = Features::F_DIM;

          // make grid
          ALUUnitCube<dim> unitcube;
          typedef ALUUnitCube<dim>::GridType Grid;
          typedef Grid::LeafGridView GV;

          for (int i=0; i<=features.maxlevel(); ++i)
            {
              const GV& gv=unitcube.grid().leafView();
              typedef Parameter<GV,double> PROBLEM;
              PROBLEM problem;
              const int degree = Features::F_DEGREE;

              if (features.method()=="SIPG") {
                  typedef Dune::PDELab::OPBLocalFiniteElementMap<Grid::ctype,double,degree,dim,Dune::GeometryType::simplex> FEMDG;
                  FEMDG femdg;
                  const int blocksize = Dune::PB::PkSize<degree,dim>::value;
                  runDG<GV,FEMDG,PROBLEM,degree,blocksize>(gv,femdg,problem,"SIMPLEX",i,"SIPG","ON",2.0);
              }

              if (features.method()=="FEM") {
                  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Grid::ctype,double,degree,dim> FEMCG;
                  FEMCG femcg(gv);
                  runFEM<GV,FEMCG,PROBLEM,degree>(gv,femcg,problem,"SIMPLEX",i);
              }

              // refine grid
              if (i<features.maxlevel()) unitcube.grid().globalRefine(1);
            }
        }
END_BLOCK(HAVE_ALUGRID)
#endif
    }
  catch (Dune::Exception &e)
    {
      std::cerr << "Dune reported error: " << e << std::endl;
      return 1;
    }
  catch (...)
    {
      std::cerr << "Unknown exception thrown!" << std::endl;
      return 1;
    }
}
