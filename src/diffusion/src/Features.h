#ifndef Features_h__included
#define Features_h__included

//Layer diffusion


//Layer mesh


//Layer simplex


//Layer dim


//Layer dim_3


//Layer method


//Layer SIPG


//Layer maxlevel


//Layer ml_4


//Layer degree


//Layer deg_1



#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/diffusion/Features.h"
class Features {
//**** Layer diffusion ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/diffusion/Features.h"


//**** Layer mesh ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/mesh/Features.h"

public:
    inline
#line 3 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/mesh/Features.h"
const std::string mesh_mesh() { return ""; }

//**** Layer simplex ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/simplex/Features.h"

public:
    const std::string mesh() { return "SIMPLEX"; }

//**** Layer dim ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/dim/Features.h"


//**** Layer dim_3 ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/dim_3/Features.h"

public:
    const static int F_DIM = 3;

//**** Layer method ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/method/Features.h"

public:
    inline
#line 3 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/method/Features.h"
const std::string method_method() { return ""; }

//**** Layer SIPG ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/SIPG/Features.h"

public:
    const std::string method() { return "SIPG"; }

//**** Layer maxlevel ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/maxlevel/Features.h"

public:
    inline
#line 3 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/maxlevel/Features.h"
int maxlevel_maxlevel() { return -1; }

//**** Layer ml_4 ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/ml_4/Features.h"

public:
    int maxlevel() { return 4; }

//**** Layer degree ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/degree/Features.h"


//**** Layer deg_1 ****
private:
#line 1 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/deg_1/Features.h"

public:
    const static int F_DEGREE = 1;

#line 2 "/home/hanna/dune-trunk/dune-pdelab-testing/src/diffusion/features/diffusion/Features.h"
};
#endif //Features_h__included
