// -*- tab-width: 4; indent-tabs-mode: nil -*-
// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file 
    \brief Solve heat equation with high order in space and time (known analytic solution)
*/
#ifdef HAVE_CONFIG_H
#include "config.h"     
#endif
#include<math.h>
#include<iostream>
#include<vector>
#include<map>
#include<string>

#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>
#include<dune/common/timer.hh>
#include<dune/common/parametertreeparser.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/grid/yaspgrid.hh>
#if HAVE_ALBERTA
#include<dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/dgfparser.hh>
#endif
#if HAVE_UG 
#include<dune/grid/uggrid.hh>
#endif
#if HAVE_ALUGRID
#include<dune/grid/alugrid.hh>
#include<dune/grid/io/file/dgfparser/dgfalu.hh>
#include<dune/grid/io/file/dgfparser/dgfparser.hh>
#endif
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/superlu.hh>

#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/finiteelementmap/p12dfem.hh>
#include<dune/pdelab/finiteelementmap/pk2dfem.hh>
#include<dune/pdelab/finiteelementmap/pk3dfem.hh>
#include<dune/pdelab/finiteelementmap/q12dfem.hh>
#include<dune/pdelab/finiteelementmap/q22dfem.hh>
#include<dune/pdelab/finiteelementmap/q1fem.hh>
#include<dune/pdelab/finiteelementmap/p1fem.hh>
#include<dune/pdelab/finiteelementmap/rannacher_turek2dfem.hh>
#include<dune/pdelab/finiteelementmap/conformingconstraints.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/constraints.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/gridoperator/onestep.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/localoperator/laplacedirichletp12d.hh>
#include<dune/pdelab/localoperator/diffusion.hh>
#include<dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include<dune/pdelab/localoperator/convectiondiffusiondg.hh>
#include<dune/pdelab/localoperator/l2.hh>
#include<dune/pdelab/newton/newton.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/instationary/onestep.hh>

#include"../utility/gridexamples.hh"
#include"../utility/utility.hh"
#include"l2interpolationerror.hh"

//==============================================================================
// Parameter class for the convection diffusion problem
//==============================================================================

const double pi = 3.141592653589793238462643;

// grid function for analytic solution at T=0.125
template<typename GV, typename RF>
class U : public Dune::PDELab::AnalyticGridFunctionBase<
  Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
  U<GV,RF> > {
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,U<GV,RF> > B;

  U (const GV& gv) : B(gv) {}
  inline void evaluateGlobal (const typename Traits::DomainType& x, 
							  typename Traits::RangeType& y) const
  {
	y = sin(2.0*pi*0.125) * sin(3.0*pi*x[0]) * sin(2.0*pi*x[1]);
  }
};

//! base class for parameter class
template<typename GV, typename RF>
class ConvectionDiffusionProblem : 
  public Dune::PDELab::ConvectionDiffusionModelProblem<GV,RF>
{
public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  //! source/reaction term
  typename Traits::RangeFieldType 
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType global = e.geometry().global(x);
    typename Traits::RangeFieldType X = sin(3.0*pi*global[0]);
    typename Traits::RangeFieldType Y = sin(2.0*pi*global[1]);
    return X*Y*(2.0*pi*cos(2.0*pi*time)+13.0*pi*pi*sin(2.0*pi*time));
    // exact solution is u(x,y,t) = sin(2*pi*t) * sin(3*pi*x) * sin(2*pi*y)
  }

  //! flux vector
  typename Traits::RangeType
  q (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType flux;
    flux[0] = 0.0;
    flux[1] = 0.0;
    return flux;
  }

  //! boundary condition type function
  // 0 means Neumann
  // 1 means Dirichlet
  // 2 means Outflow (zero diffusive flux)
  int
  bc (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 1;
    typename Traits::RangeType global = is.geometry().global(x);
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType 
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! Neumann boundary condition
  // Good: The dependence on u allows us to implement Robin type boundary conditions.
  // Bad: This interface cannot be used for mixed finite elements where the flux is the essential b.c.
  typename Traits::RangeFieldType 
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! set time for subsequent evaluation
  void setTime (RF t)
  {
    time = t;
  }

private:
  RF time;
};

//===============================================================
// Some variants to solve the nonlinear diffusion problem
//===============================================================

// a sequential variant
template<class GV>
void sequential_FEM (const GV& gv, int t_level)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  //typedef Dune::PDELab::Q1LocalFiniteElementMap<Coord,Real,dim> FEM;
  typedef Dune::PDELab::Q22DLocalFiniteElementMap<Coord,Real> FEM;
  FEM fem;
  typedef Dune::PDELab::ConformingDirichletConstraints CON;
  typedef Dune::PDELab::ISTLVectorBackend<1> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE,
    Dune::PDELab::SimpleGridFunctionStaticSize> GFS;
  GFS gfs(gv,fem);

  // <<<2b>>> define problem parameters
  typedef ConvectionDiffusionProblem<GV,Real> Param;
  Param param;
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Param> B;
  B b(gv,param);
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Param> G;
  G g(gv,param);

  // <<<3>>> Compute constrained space
  typedef typename GFS::template ConstraintsContainer<Real>::Type C;
  C cg;
  Dune::PDELab::constraints(b,gfs,cg);
  std::cout << "constrained dofs=" << cg.size() 
            << " of " << gfs.globalSize() << std::endl;

  // <<<5>>> Make grid operator space for time-dependent problem
  typedef Dune::PDELab::ConvectionDiffusionFEM<Param,FEM> LOP; 
  LOP lop(param,4);
  typedef Dune::PDELab::L2 MLOP; 
  MLOP mlop(4);
  typedef VBE::MatrixBackend MBE;
  Dune::PDELab::FractionalStepParameter<Real> method;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,C,C> GO0;
  GO0 go0(gfs,cg,gfs,cg,lop);
  typedef Dune::PDELab::GridOperator<GFS,GFS,MLOP,MBE,Real,Real,Real,C,C> GO1;
  GO1 go1(gfs,cg,gfs,cg,mlop);
  typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
  IGO igo(go0,go1);
  typedef typename IGO::Traits::Domain V;

  // <<<6>>> Make a linear solver 
  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  LS ls(5000,1);

  // <<<7>>> make Newton for time-dependent problem
  typedef Dune::PDELab::Newton<IGO,LS,V> PDESOLVER;
  PDESOLVER tnewton(igo,ls);
  tnewton.setReassembleThreshold(0.0);
  tnewton.setVerbosityLevel(0);
  tnewton.setReduction(0.9);
  tnewton.setMinLinearReduction(1e-9);

  // <<<8>>> time-stepper
  Dune::PDELab::OneStepMethod<Real,IGO,PDESOLVER,V,V> osm(method,igo,tnewton);
  osm.setVerbosityLevel(2);

  // <<<9>>> initial value and initial value for first time step with b.c. set
  V xold(gfs,0.0);
  xold = 0.0;

  // <<<10>>> graphics for initial guess
  std::stringstream fullname;
  fullname << "instationarytest_FEM_Q1_t_level" << t_level;
  Dune::PDELab::FilenameHelper fn(fullname.str());
  {
    typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
    DGF xdgf(gfs,xold);
    Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
    vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(xdgf,"solution"));
    vtkwriter.write(fn.getName(),Dune::VTK::ascii);
    fn.increment();
  }

  // <<<11>>> time loop
  Real time = 0.0;
  int N=1; for (int i=0; i<t_level; i++) N *= 2;
  Real dt = 0.125/N;
  V x(gfs,0.0);
  param.setTime(dt);
  Dune::PDELab::interpolate(g,gfs,x);
  Dune::PDELab::set_nonconstrained_dofs(cg,0.0,x);
  for (int i=1; i<=N; i++)
    {
      // do time step
      osm.apply(time,dt,xold,x);

      // graphics
      typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
      DGF xdgf(gfs,x);
      Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
      vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(xdgf,"solution"));
      vtkwriter.write(fn.getName(),Dune::VTK::ascii);
      fn.increment();

      // advance time step
//       std::cout.precision(8);
//       std::cout << "solution maximum: " 
//                 << std::scientific << x.infinity_norm() << std::endl;
      xold = x;
      time += dt;
    }

  // evaluate discretization error
  U<GV,Real> u(gv);
  std::cout.precision(8);
  TEST_OUTPUT("space time discretization error", std::setw(8) << std::scientific << l2interpolationerror(u,gfs,x,8))
  {
    Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
    vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<U<GV,Real> >(u,"exact solution"));
    std::stringstream fullname;
    fullname << "instationarytest_FEM_exact_t_level" << t_level;
    vtkwriter.write(fullname.str(),Dune::VTK::ascii);
  }
}

// a sequential variant
template<class GV>
void sequential_DG (const GV& gv, int t_level, std::string dg_method, std::string weights, double alpha)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  //typedef Dune::PDELab::Q1LocalFiniteElementMap<Coord,Real,dim> FEM;
  typedef Dune::PDELab::Q22DLocalFiniteElementMap<Coord,Real> FEM;
  FEM fem;
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTLVectorBackend<1> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE,
    Dune::PDELab::SimpleGridFunctionStaticSize> GFS;
  GFS gfs(gv,fem);

  // <<<2b>>> define problem parameters
  typedef ConvectionDiffusionProblem<GV,Real> Param;
  Param param;
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Param> B;
  B b(gv,param);
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Param> G;
  G g(gv,param);

  // <<<3>>> Compute constrained space
  typedef typename GFS::template ConstraintsContainer<Real>::Type C;
  C cg;
  Dune::PDELab::constraints(b,gfs,cg);
  std::cout << "constrained dofs=" << cg.size() 
            << " of " << gfs.globalSize() << std::endl;

  // <<<5>>> Make grid operator space for time-dependent problem
  Dune::PDELab::ConvectionDiffusionDGMethod::Type m;
  if (dg_method=="SIPG") m = Dune::PDELab::ConvectionDiffusionDGMethod::SIPG;
  if (dg_method=="NIPG") m = Dune::PDELab::ConvectionDiffusionDGMethod::NIPG;
  Dune::PDELab::ConvectionDiffusionDGWeights::Type w;
  if (weights=="ON") w = Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn;
  if (weights=="OFF") w = Dune::PDELab::ConvectionDiffusionDGWeights::weightsOff;
  typedef Dune::PDELab::ConvectionDiffusionDG<Param,FEM> LOP;
  LOP lop(param,m,w,alpha);
  typedef Dune::PDELab::L2 MLOP; 
  MLOP mlop(4);
  //typedef Dune::PDELab::ISTLBCRSMatrixBackend<1,1> MBE;
  typedef VBE::MatrixBackend MBE;
  Dune::PDELab::FractionalStepParameter<Real> timestep_method;
//  typedef typename GFS::template VectorContainer<Real>::Type V;
//  typedef Dune::PDELab::InstationaryGridOperatorSpace<Real,V,GFS,GFS,LOP,MLOP,C,C,MBE> IGOS;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,C,C> GO0;
  GO0 go0(gfs,cg,gfs,cg,lop);
  typedef Dune::PDELab::GridOperator<GFS,GFS,MLOP,MBE,Real,Real,Real,C,C> GO1;
  GO1 go1(gfs,cg,gfs,cg,mlop);
  typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
//  IGOS igos(timestep_method,gfs,cg,gfs,cg,lop,mlop);
  IGO igo(go0,go1);
  typedef typename IGO::Traits::Domain V;

  // <<<6>>> Make a linear solver 
// #if HAVE_SUPERLU
//   typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LS;
//   LS ls(false);
// #else
  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  LS ls(5000,1);
  //#endif

  // <<<7>>> make Newton for time-dependent problem
  typedef Dune::PDELab::Newton<IGO,LS,V> PDESOLVER;
  PDESOLVER tnewton(igo,ls);
  tnewton.setReassembleThreshold(0.0);
  tnewton.setVerbosityLevel(0);
  tnewton.setReduction(0.9);
  tnewton.setMinLinearReduction(1e-9);

  // <<<8>>> time-stepper
  Dune::PDELab::OneStepMethod<Real,IGO,PDESOLVER,V,V> osm(timestep_method,igo,tnewton);
  osm.setVerbosityLevel(2);

  // <<<9>>> initial value and initial value for first time step with b.c. set
  V xold(gfs,0.0);
  xold = 0.0;

  // <<<10>>> graphics for initial guess
  std::stringstream fullname;
  fullname << "instationarytest_" << dg_method << "_Q1_t_level" << t_level;
  Dune::PDELab::FilenameHelper fn(fullname.str());
  {
    typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
    DGF xdgf(gfs,xold);
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
    vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(xdgf,"solution"));
    vtkwriter.write(fn.getName(),Dune::VTK::ascii);
    fn.increment();
  }

  // <<<11>>> time loop
  Real time = 0.0;
  int N=1; for (int i=0; i<t_level; i++) N *= 2;
  Real dt = 0.125/N;
  V x(gfs,0.0);
  param.setTime(dt);
  Dune::PDELab::interpolate(g,gfs,x);
  Dune::PDELab::set_nonconstrained_dofs(cg,0.0,x);
  for (int i=1; i<=N; i++)
    {
      // do time step
      osm.apply(time,dt,xold,x);

      // graphics
      typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
      DGF xdgf(gfs,x);
      Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
      vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(xdgf,"solution"));
      vtkwriter.write(fn.getName(),Dune::VTK::ascii);
      fn.increment();

      // advance time step
//       std::cout.precision(8);
//       std::cout << "solution maximum: " 
//                 << std::scientific << x.infinity_norm() << std::endl;
      xold = x;
      time += dt;
    }

  // evaluate discretization error
  U<GV,Real> u(gv);
  std::cout.precision(8);
  TEST_OUTPUT("space time discretization error", std::setw(8) << std::scientific << l2interpolationerror(u,gfs,x,8))
  {
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
    vtkwriter.addCellData(new Dune::PDELab::VTKGridFunctionAdapter<U<GV,Real> >(u,"exact solution"));
    std::stringstream fullname;
    fullname << "instationarytest_" << dg_method << "_exact_t_level" << t_level;
    vtkwriter.write(fullname.str(),Dune::VTK::ascii);
  }
}

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
	  {
		if(helper.rank()==0)
		  std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
	  }

    if (argc!=2)
      {
        if(helper.rank()==0)
          std::cout << "usage: ./executable <config file>" << std::endl;
        return 1;
      }
    // Parse configuration file.
    Dune::ParameterTree paramtree;
    try{
      Dune::ParameterTreeParser::readINITree(argv[1],paramtree);
    }
    catch(...){
      std::cerr << "Could not read config file \"" 
                << std::string(argv[1]) << "\"!" << std::endl;
      exit(1);
    }
    const int t_level(paramtree.get<int>("domain.t_level"));
    const int x_level(paramtree.get<int>("domain.x_level"));

    // sequential version
    {
      Dune::FieldVector<double,2> L(1.0);
      Dune::FieldVector<int,2> N(16);
      Dune::FieldVector<bool,2> periodic(false);
      int overlap=0;
      Dune::YaspGrid<2> grid(L,N,periodic,overlap);
      grid.globalRefine(x_level);
      typedef Dune::YaspGrid<2>::LeafGridView GV;
      const GV& gv=grid.leafView();

      const std::string method = paramtree.get<std::string>("domain.method");
      if (method == "FEM")
      {
        sequential_FEM(gv,t_level);
      }
      if (method == "SIPG")
      {
        sequential_DG(gv,t_level,"SIPG","ON",2.0);
      }
    }
  }

  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
	return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
	return 1;
  }
}
