// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file 
    \brief Solve Poisson problem on various grids (sequential)
*/
#ifdef HAVE_CONFIG_H
#include "config.h"     
#endif
#include<iostream>
#include<vector>
#include<map>
#include<string>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/float_cmp.hh>
#include<dune/common/static_assert.hh>
#include<dune/common/parametertreeparser.hh>
#include<dune/grid/yaspgrid.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>

#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/finiteelementmap/p1fem.hh>
#include<dune/pdelab/finiteelementmap/pk2dfem.hh>
#include<dune/pdelab/finiteelementmap/pk3dfem.hh>
#include<dune/pdelab/finiteelementmap/q22dfem.hh>
#include<dune/pdelab/finiteelementmap/q1fem.hh>
#include<dune/pdelab/finiteelementmap/conformingconstraints.hh>
#include<dune/pdelab/finiteelementmap/hangingnodeconstraints.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/constraints/constraints.hh>
#include<dune/pdelab/common/function.hh>
#include <dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/localoperator/laplacedirichletp12d.hh>
#include<dune/pdelab/localoperator/poisson.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>

#include "../utility/gridexamples.hh"
#include "../utility/gridfunction.hh"
#include"../utility/utility.hh"

//===============================================================
//===============================================================
// Solve the Poisson equation
//           - \Delta u = f in \Omega, 
//                    u = g on \partial\Omega_D
//  -\nabla u \cdot \nu = j on \partial\Omega_N
//===============================================================
//===============================================================

//===============================================================
// Define parameter functions f,g,j and \partial\Omega_D/N
//===============================================================

// function for defining the source term
template<typename GV, typename RF>
class F
  : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
                                                  F<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,F<GV,RF> > BaseT;

  F (const GV& gv) : BaseT(gv) {}
  inline void evaluateGlobal (const typename Traits::DomainType& x, 
							  typename Traits::RangeType& y) const
  {
#if 0
    if (x[0]>0.25 && x[0]<0.375 && x[1]>0.25 && x[1]<0.375)
      y = 50.0;
    else
      y = 0.0;
#endif
    y=0;
  }
};

// constraints parameter class for selecting boundary condition type
class BCTypeParam
  : public Dune::PDELab::DirichletConstraintsParameters /*@\label{bcp:base}@*/
{
public:

  template<typename I>
  bool isDirichlet(
				   const I & intersection,   /*@\label{bcp:name}@*/
				   const Dune::FieldVector<typename I::ctype, I::dimension-1> & coord
				   ) const
  {
#if 1
  return true;  // Dirichlet b.c. everywhere
#else
    Dune::FieldVector<typename I::ctype, I::dimension>
      xg = intersection.geometry().global( coord );
	
    if( xg[1]<1E-6 || xg[1]>1.0-1E-6 )
      return false; // Neumann b.c.
    else if( xg[0]>1.0-1E-6 && xg[1]>0.5+1E-6 )
      return false; // Neumann b.c.
    else
      return true;  // Dirichlet b.c. on all other boundaries
#endif
}
};

// function for Dirichlet boundary conditions and initialization
template<typename GV, typename RF>
class G
  : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
                                                  G<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,G<GV,RF> > BaseT;
  typedef typename Traits::DomainType DomainType;

  G (const GV& gv) : BaseT(gv), center(-0.5) {}
  
  inline void evaluateGlobal (const typename Traits::DomainType& x, 
							  typename Traits::RangeType& y) const
  {
    typename Traits::DomainType xg(x); 
    xg -= center;
    if(GV::dimension == 2)
      y = std::log(std::sqrt(xg * xg));
    else
      y = - 1.0 / std::sqrt(xg * xg);
  }

private:
  const DomainType center;
};

// function for defining the flux boundary condition
template<typename GV, typename RF>
class J
  : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
                                                  J<GV,RF> >
{
public:
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,J<GV,RF> > BaseT;

  J (const GV& gv) : BaseT(gv) {}
  inline void evaluateGlobal (const typename Traits::DomainType& x, 
							  typename Traits::RangeType& y) const
  {
    if (x[1]<1E-6 || x[1]>1.0-1E-6)
      {
        y = 0;
        return;
      }
    if (x[0]>1.0-1E-6 && x[1]>0.5+1E-6)
      {
        y = -5.0;
        return;
      }
  }
};

//===============================================================
// Problem Solver
//===============================================================

/**
   Solver for the poisson equation

   \tparam GV The grid view
   \tparam FEM The finite element map
   \tparam BF The boundary condition type function
   \tparam CON The constraints assembler
   \tparam q Order of local quadrature

*/
template<typename GV, typename FEM, typename CON, int q> 
class PoissonSolver
{
public:
  typedef double Real;
  //! Grid view
  typedef GV GridView;
  
  //! Field types
  //! @{
  typedef typename GV::Grid::ctype DF;
  typedef typename FEM::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;
  //! @}

  //! Grid function space
  typedef Dune::PDELab::ISTLVectorBackend<1> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;

  //! Constraints container
  typedef typename GFS::template ConstraintsContainer<RF>::Type C;

  typedef G<GV,RF> GType;
  typedef F<GV,RF> FType;
  typedef J<GV,RF> JType;
  typedef Dune::PDELab::Poisson<FType,BCTypeParam,JType,q> LOP;
  typedef VBE::MatrixBackend MBE;
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,C,C> GO;

  /** \brief Constructor

      All parameters passed to the constructor are expected to refer
      to dynamically allocated objects. This class takes ownership of
      all these objects.

      \param [in] gv_ The grid view 
      \param [in] fem_ The finite element map
      \param [in] bf_ The boundary type function
      \param [in] con_ The constraints assembler

   */
  PoissonSolver(const GV * gv_, const FEM * fem_, const CON * con_ = new CON())
    : gv(*gv_), fem(*fem_), con(*con_), gfs(*(new GFS(gv,fem,con))),
      x0(*(new V(gfs))), dgf(*(new DGF(gfs,x0)))
  {
    // Assembler constraints
    C cg; cg.clear();

    BCTypeParam bctype; // boundary condition type
    Dune::PDELab::constraints(bctype,gfs,cg, false);
    
    // Reset coefficient vector
    x0 = 0.0;

    // Dirichlet function with singularity (at <center>)
    GType g(gv);
    Dune::PDELab::interpolate(g,gfs,x0);
    Dune::PDELab::set_shifted_dofs(cg,0.0,x0);

    // Source function
    FType f(gv);

    // Neumann function
    JType j(gv);

    // Local operator
    LOP lop(f,bctype,j);

    // Grid operator
    GO go(gfs,cg,gfs,cg,lop);

    // represent operator as a matrix
    typedef typename GO::Jacobian M;
    M m(go);
    m = 0.0;

    // For hangingnodes: Interpolate hangingnodes adajcent to dirichlet
    // nodes
    go.localAssembler().backtransform(x0);
    go.jacobian(x0,m);
    //  Dune::printmatrix(std::cout,m.base(),"global stiffness matrix","row",9,1);

    // evaluate residual w.r.t initial guess
    V r(gfs);
    r = 0.0;
    go.residual(x0,r);

    // make ISTL solver
    Dune::MatrixAdapter<M,V,V> opa(m);
    Dune::SeqSSOR<M,V,V> ssor(m,1,1.0);
    Dune::CGSolver<V> solvera(opa,ssor,1E-10,5000,1);
    Dune::InverseOperatorResult stat;

    // solve the jacobian system
    r *= -1.0; // need -residual
    V x(gfs,0.0);
    solvera.apply(x,r,stat);

    // For hangingnodes: Set values of hangingnodes to zero
    Dune::PDELab::set_shifted_dofs(cg,0.0,x0);

    // Affine shift
    x += x0; 

    // Transform solution into standard basis
    go.localAssembler().backtransform(x);

    // Store solution
    x0 = x;
  }

  //! DOF coefficient vector
  //typedef typename GFS::template VectorContainer<RF>::Type V;
  typedef typename PoissonSolver::GO::Traits::Domain V;

  //! Grid function
  typedef Dune::PDELab::DiscreteGridFunction<GFS,V> DGF;
  typedef DGF SolutionGridFunction;

  // Output solution as vtk file
  void writeVTKFiles(std::string filename){
    Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
    vtkwriter.addVertexData(new Dune::PDELab::VTKGridFunctionAdapter<DGF>(dgf,"solution"));
    vtkwriter.write(filename,Dune::VTK::ascii);
  }

  //! Access to solution as a grid function
  const SolutionGridFunction & solutionGridFunction() const {
    return dgf;
  }
  
  ~PoissonSolver(){
    delete &dgf;
    delete &x0;
    delete &gfs;
    delete &fem;
    delete &con;
    delete &gv;
  }

private:
  const GV & gv;                // Grid view
  const FEM & fem;              // Finite element map
  const CON & con;              // Constraints assembler
  GFS  & gfs;                   // Grid function space
  V & x0;                       // Coefficient vector
  DGF & dgf;                    // Discrete Grid Function

};

template<typename GV, typename Solver>
void computel2error(const GV & gv, Solver & solver, const int level, const int q, double & error){

  // Reference solution
  typedef G<GV,typename Solver::RF> Reference; 
  Reference reference(gv);

  // Compute l2 error
  typedef DifferenceSquaredAdapter< typename Solver::SolutionGridFunction, Reference> 
    ErrorGridFunction;
  ErrorGridFunction l2errorfunction(solver.solutionGridFunction(),reference);
  typename ErrorGridFunction::Traits::RangeType l2error(0);
  Dune::PDELab::integrateGridFunction(l2errorfunction,l2error,2*q);
  TEST_OUTPUT("L2-Error for refinement " << level, std::scientific << l2error << std::resetiosflags(std::ios::scientific))
  error = l2error;
}

template<typename Errors>
void outputl2errors(Errors & errors){
  for(unsigned int l=0; l+1 <errors.size(); ++l){
    std::cout << "Convergence rate from level " << l << " to level " << l+1 
              << " : " << std::log(errors[l]/errors[l+1]) / std::log(2.0) 
              << std::endl;
  }
}

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper::instance(argc, argv);

    // Parse configuration file.
    std::string config_file(argv[1]);
    Dune::ParameterTree parameters;
    try{
      Dune::ParameterTreeParser::readINITree(config_file, parameters);
    }
    catch(...){
      std::cerr << "Could not read config file \"" 
                << config_file << "\"!" << std::endl;
      exit(1);
    }


#if HAVE_ALUGRID
BEGIN_BLOCK(HAVE_ALUGRID)
    // unit square with uniform refinement
    {
      std::cout << "ALU grid 2d unit square test:" << std::endl;

      const int n_level(parameters.get<int>("alu2dp3.max_level"));
      std::vector<double> l2error(n_level);

      // make grid 
      ALUUnitSquare grid;
      grid.globalRefine(n_level);
      
      for(int l=0; l<n_level; ++l){
        // get view
        typedef ALUUnitSquare::LevelGridView GV;
        const GV * gv= new GV(grid.levelView(l)); 
      
        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef double R;
        const int k=3;
        const int q=2*k;
        typedef Dune::PDELab::Pk2DLocalFiniteElementMap<GV,DF,double,k> FEM;
        FEM * fem = new FEM(*gv);

        // solve equation
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        typedef PoissonSolver<GV,FEM,CON,q> Solver;
        Solver solver(gv,fem);
        std::stringstream str; str << "poisson_ALU_Pk_2d_" << l;
        solver.writeVTKFiles(str.str());

        computel2error(*gv,solver,l,q,l2error[l]);
      }
      outputl2errors(l2error);
    }

    // unit cube with hanging node refinement
    {
      std::cout << "ALU grid 3d hanging node test:" << std::endl;

      const int n_level(parameters.get<int>("alu3dhangingnodes.max_level"));
      std::vector<double> l2error(n_level);

      // make grid 
      ALUCubeUnitSquare grid;
      grid.globalRefine(1);
      
      typedef ALUCubeUnitSquare::Codim<0>::Partition<Dune::All_Partition>::LeafIterator 
        Iterator;
      typedef ALUCubeUnitSquare::LeafIntersectionIterator IntersectionIterator;
      typedef ALUCubeUnitSquare::LeafGridView GV;
      typedef ALUCubeUnitSquare::ctype ctype;

      // Do some random refinement. The result is a grid that may
      // contain multiple hanging nodes per edge.
      for(int i=0; i<n_level;++i){
        Iterator it = grid.leafbegin<0,Dune::All_Partition>();
        Iterator eit = grid.leafend<0,Dune::All_Partition>();

        for(;it!=eit;++it){
          if((double)rand()/(double)RAND_MAX > 0.6)
            grid.mark(1,*(it));
        }
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();

        // get view
        const GV * pgv=new GV(grid.leafView()); 
        const GV & gv = *pgv;
      
        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef double R;
        const int q=2;
        typedef Dune::PDELab::Q1LocalFiniteElementMap<DF,R,3> FEM;
        FEM * fem = new FEM();

        // We need the boundary function for the hanging nodes
        // constraints engine as we have to distinguish between hanging
        // nodes on dirichlet and on neumann boundaries
        BCTypeParam bctype;

        // This is the type of the local constraints assembler that has
        // to be adapted for different local basis spaces and grid types
        typedef Dune::PDELab::HangingNodesConstraintsAssemblers::CubeGridQ1Assembler ConstraintsAssembler;

        // The type of the constraints engine
        typedef Dune::PDELab::HangingNodesDirichletConstraints
          <GV::Grid,ConstraintsAssembler,BCTypeParam> Constraints;

        // Get constraints engine. We set adaptToIsolateHangingNodes =
        // true as ALU Grid refinement allows for the appearance of
        // multiple hanging nodes per edge.
        Constraints * constraints = new Constraints(grid,true,bctype);
      
        // Solve equation
        typedef PoissonSolver<GV,FEM,Constraints,q> Solver;
        Solver solver(pgv,fem,constraints);
        std::stringstream str; str << "poisson_ALU_Q1_3d_" << i;
        solver.writeVTKFiles(str.str());

        computel2error(*pgv,solver,i,q,l2error[i]);
      }
      outputl2errors(l2error);
    }
END_BLOCK(HAVE_ALUGRID)
#endif


    // ALU Pk 3D test
#if HAVE_ALUGRID
BEGIN_BLOCK(HAVE_ALUGRID)
    {
      std::cout << "ALU grid 3d unit cube test:" << std::endl;

      const int n_level(parameters.get<int>("alu3dp4.max_level"));
      std::vector<double> l2error(n_level);

      // make grid
      typedef ALUUnitCube<3> UnitCube;
      UnitCube unitcube;
      unitcube.grid().globalRefine(n_level);

      for(int l=0; l<n_level; ++l){
        // get view
        typedef UnitCube::GridType::LevelGridView GV;
        const GV * gv= new GV(unitcube.grid().levelView(l)); 
 
        // make finite element map
        typedef UnitCube::GridType::ctype DF;
        typedef double R;
        const int k=4;
        const int q=2*k;
        typedef Dune::PDELab::Pk3DLocalFiniteElementMap<GV,DF,R,k> FEM;
        FEM * fem = new FEM(*gv);

        // solve equation
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        typedef PoissonSolver<GV,FEM,CON,q> Solver;
        Solver solver(gv,fem);
        std::stringstream str; str << "poisson_ALU_Pk_3d_" << l;
        solver.writeVTKFiles(str.str());

        computel2error(*gv,solver,l,q,l2error[l]);
      }
      outputl2errors(l2error);
    }
END_BLOCK(HAVE_ALUGRID)
#endif

    // YaspGrid Q1 2D test
    {
      std::cout << "YASP grid 2d unit square Q1 test:" << std::endl;

      const int n_level(parameters.get<int>("yasp2dq1.max_level"));
      std::vector<double> l2error(n_level);

      // make grid
      Dune::FieldVector<double,2> L(1.0);
      Dune::FieldVector<int,2> N(1);
      Dune::FieldVector<bool,2> P(false);
      Dune::YaspGrid<2> grid(L,N,P,0);
      grid.globalRefine(n_level);

      for(int l=0; l<n_level; ++l){
        // get view
        typedef Dune::YaspGrid<2>::LevelGridView GV;
        const GV * gv= new GV(grid.levelView(l)); 

        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef Dune::PDELab::Q1LocalFiniteElementMap<DF,double,2> FEM;
        FEM * fem = new FEM();
        const int q = 2;
  
        // solve equation
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        typedef PoissonSolver<GV,FEM,CON,q> Solver;
        Solver solver(gv,fem);
        std::stringstream str; str << "poisson_yasp_Q1_2d_" << l;
        solver.writeVTKFiles(str.str());

        computel2error(*gv,solver,l,q,l2error[l]);
      }
      outputl2errors(l2error);
    }

    // YaspGrid Q2 2D test
    {
      std::cout << "YASP grid 2d unit square Q2 test:" << std::endl;

      const int n_level(parameters.get<int>("yasp2dq2.max_level"));
      std::vector<double> l2error(n_level);

      // make grid
      Dune::FieldVector<double,2> L(1.0);
      Dune::FieldVector<int,2> N(1);
      Dune::FieldVector<bool,2> P(false);
      Dune::YaspGrid<2> grid(L,N,P,0);
      grid.globalRefine(n_level);

      for(int l=0; l<n_level; ++l){
        // get view
        typedef Dune::YaspGrid<2>::LevelGridView GV;
        const GV * gv= new GV(grid.levelView(l)); 

        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef Dune::PDELab::Q22DLocalFiniteElementMap<DF,double> FEM;
        FEM * fem = new FEM();
        const int q = 4;

        // solve equation
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        typedef PoissonSolver<GV,FEM,CON,q> Solver;
        Solver solver(gv,fem);
        std::stringstream str; str << "poisson_yasp_Q2_2d_" << l;
        solver.writeVTKFiles(str.str());
      }
    }

    // YaspGrid Q1 3D test
    {
      std::cout << "YASP grid 3d unit cube Q1 test:" << std::endl;

      const int n_level(parameters.get<int>("yasp3dq1.max_level"));
      std::vector<double> l2error(n_level);

      // make grid
      Dune::FieldVector<double,3> L(1.0);
      Dune::FieldVector<int,3> N(1);
      Dune::FieldVector<bool,3> P(false);
      Dune::YaspGrid<3> grid(L,N,P,0);
      grid.globalRefine(n_level);

      for(int l=0; l<n_level; ++l){

        // get view
        typedef Dune::YaspGrid<3>::LevelGridView GV;
        const GV * gv= new GV(grid.levelView(l)); 

        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef Dune::PDELab::Q1LocalFiniteElementMap<DF,double,3> FEM;
        FEM * fem = new FEM();
        const int q = 2;

        // solve equation
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        typedef PoissonSolver<GV,FEM,CON,q> Solver;
        Solver solver(gv,fem);
        std::stringstream str; str << "poisson_yasp_Q1_3d_" << l;
        solver.writeVTKFiles(str.str());

        computel2error(*gv,solver,l,q,l2error[l]);
      }
      outputl2errors(l2error);
    }

    // UG Pk 2D test
#if HAVE_UG
BEGIN_BLOCK(HAVE_UG)
    {
      std::cout << "UG grid 2d unit square P3 test:" << std::endl;

      const int n_level(parameters.get<int>("ug2dp3.max_level"));
      std::vector<double> l2error(n_level);

      // make grid 
      UGUnitSquare grid;
      grid.globalRefine(n_level);

      for(int l=0; l<n_level; ++l){

        // get view
        typedef UGUnitSquare::LevelGridView GV;
        const GV * gv = new GV(grid.levelView(l)); 
 
        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef double R;
        const int k=3;
        const int q=2*k;
        typedef Dune::PDELab::Pk2DLocalFiniteElementMap<GV,DF,double,k> FEM;
        FEM * fem = new FEM(*gv);
  
        // solve equation
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        typedef PoissonSolver<GV,FEM,CON,q> Solver;
        Solver solver(gv,fem);
        std::stringstream str; str << "poisson_UG_Pk_2d" << l;
        solver.writeVTKFiles(str.str());

        computel2error(*gv,solver,l,q,l2error[l]);
      }
      outputl2errors(l2error);
    }

    {
      std::cout << "UG grid 3d hanging nodes test:" << std::endl;

      const int n_level(parameters.get<int>("ug3dhangingnodes.max_level"));
      std::vector<double> l2error(n_level);

      // get grid and do a single global refine
      UGUnitCube<3,1> ugunitcube;
      typedef UGUnitCube<3,1>::GridType Grid;
      Grid & grid = ugunitcube.grid();
      grid.setRefinementType(Grid::LOCAL);
      grid.setClosureType(Grid::NONE);
      grid.globalRefine(1);

      typedef Grid::Codim<0>::Partition<Dune::All_Partition>::LeafIterator 
        Iterator;
      typedef Grid::LeafIntersectionIterator IntersectionIterator;
      typedef Grid::LeafGridView GV;
      typedef Grid::ctype ctype;
      
       // Do some random refinement. The result is a grid that may
      // contain multiple hanging nodes per edge.
      for(int i=0; i<n_level;++i){
        Iterator it = grid.leafbegin<0,Dune::All_Partition>();
        Iterator eit = grid.leafend<0,Dune::All_Partition>();

        for(;it!=eit;++it){
          if((double)rand()/(double)RAND_MAX > 0.6)
            grid.mark(1,*(it));
        }
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();

        // get view
        const GV * gv= new GV(grid.leafView()); 

        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef double R;
        const int q=2;
        typedef Dune::PDELab::Q1LocalFiniteElementMap<DF,R,3> FEM;
        FEM * fem = new FEM();

        // We need the boundary function for the hanging nodes
        // constraints engine as we have to distinguish between hanging
        // nodes on dirichlet and on neumann boundaries
        BCTypeParam bctype;

        // This is the type of the local constraints assembler that has
        // to be adapted for different local basis spaces and grid types
        typedef Dune::PDELab::HangingNodesConstraintsAssemblers::CubeGridQ1Assembler ConstraintsAssembler;

        // The type of the constraints engine
        typedef Dune::PDELab::HangingNodesDirichletConstraints
          <GV::Grid,ConstraintsAssembler,BCTypeParam> Constraints;

        // Get constraints engine. We set adaptToIsolateHangingNodes =
        // true and therefore the constructor refines the grid until
        // there are fewer than one hanging node per edge.
        Constraints *constraints = new Constraints(grid,true,bctype);

        // Solve equation
        typedef PoissonSolver<GV,FEM,Constraints,q> Solver;
        Solver solver(gv,fem,constraints);
        std::stringstream str; str << "poisson_UG_Q1_3d" << i;
        solver.writeVTKFiles(str.str());
        
        computel2error(*gv,solver,i,q,l2error[i]);
      }
      outputl2errors(l2error);
    }
END_BLOCK(HAVE_UG)
#endif


#if HAVE_ALBERTA
BEGIN_BLOCK(HAVE_ALBERTA)
    {
      std::cout << "Alberta grid 2d unit square P3 test:" << std::endl;

      const int n_level(parameters.get<int>("alberta2dp3.max_level"));
      std::vector<double> l2error(n_level);

      // make grid 
      AlbertaUnitSquare grid;
      grid.globalRefine(n_level);
      
      for(int l=0; l<n_level; ++l){
        // get view
        typedef AlbertaUnitSquare::LevelGridView GV;
        const GV* gv = new GV(grid.levelView()); 
      
        // make finite element map
        typedef GV::Grid::ctype DF;
        typedef double R;
        const int k=3;
        const int q=2*k;
        typedef Dune::PDELab::Pk2DLocalFiniteElementMap<GV,DF,double,k> FEM;
        FEM * fem = new FEM(gv);

        // solve equation
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        typedef PoissonSolver<GV,FEM,CON,q> Solver;
        Solver solver(gv,fem);
        std::stringstream str; str << "poisson_Alberta_Pk_2d" << l;
        solver.writeVTKFiles(str.str());

        computel2error(*gv,solver,l,q,l2error[l]);
      }
      outputl2errors(l2error);
    }
END_BLOCK(HAVE_ALBERTA)
#endif


	// test passed
	return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
	return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
	return 1;
  }
}
