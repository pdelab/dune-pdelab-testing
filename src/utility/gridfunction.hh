#ifndef DUNE_PDELAB_TESTING_UTILITY_GRIDFUNCTION_HH
#define DUNE_PDELAB_TESTING_UTILITY_GRIDFUNCTION_HH

/*! \brief Adapter returning ||f1(x)-f2(x)||^2 for two given grid functions

  \tparam T1  a grid function type
  \tparam T2  a grid function type
*/
template<typename T1, typename T2>
class DifferenceSquaredAdapter 
  : public Dune::PDELab::GridFunctionBase<
  Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
				   typename T1::Traits::RangeFieldType,
				   1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> >
  ,DifferenceSquaredAdapter<T1,T2> >
{
public:
  typedef Dune::PDELab::GridFunctionTraits<typename T1::Traits::GridViewType,
                                           typename T1::Traits::RangeFieldType,
                                           1,Dune::FieldVector<typename T1::Traits::RangeFieldType,1> > Traits;

  //! constructor 
  DifferenceSquaredAdapter (const T1& t1_, const T2& t2_) : t1(t1_), t2(t2_) {}

  //! \copydoc GridFunctionBase::evaluate()
  inline void evaluate (const typename Traits::ElementType& e, 
                        const typename Traits::DomainType& x, 
                        typename Traits::RangeType& y) const
  {  
    typename T1::Traits::RangeType y1;
    t1.evaluate(e,x,y1);
    typename T2::Traits::RangeType y2;
    t2.evaluate(e,x,y2);
    y1 -= y2;
    y = y1.two_norm2();
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return t1.getGridView();
  }
  
private:
  const T1& t1;
  const T2& t2;
};

/**
   \brief This grid function computes the squared difference of two given
   grid functions. 

   The functions are expected to be defined on level grid views and
   the reference function is expected to be defined on a higher
   level. The evaluate function should only be called for elements
   from the grid view of the reference grid function.

  \tparam RF Reference grid function (on a fine level)
  \tparam TF Test grid function (on a coarser level than RF)
 */
template<typename RF, typename TF>
class MultiLevelDifferenceSquaredAdapter
  : public Dune::PDELab::TypeTree::LeafNode, Dune::PDELab::GridFunctionInterface<
  Dune::PDELab::GridFunctionTraits<
    typename RF::Traits::GridViewType,
    typename RF::Traits::RangeFieldType,
    1,
    Dune::FieldVector<typename RF::Traits::RangeFieldType,1>
    >,
  MultiLevelDifferenceSquaredAdapter<RF,TF>
  >
{
public:

  typedef Dune::PDELab::GridFunctionTraits<
  typename RF::Traits::GridViewType,
  typename RF::Traits::RangeFieldType,
  1,
  Dune::FieldVector<typename RF::Traits::RangeFieldType,1>
  > Traits;

  MultiLevelDifferenceSquaredAdapter (const RF & rf_, const TF & tf_)
    : rf(rf_), tf(tf_), tf_level(tf.getGridView().template begin<0>()->level()),
      rf_level(rf.getGridView().template begin<0>()->level())
  {}

  // Evaluate
  inline void evaluate (const typename Traits::ElementType& e, 
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {  
    assert(e.level() == rf_level);
    typename RF::Traits::RangeType ry;
    rf.evaluate(e,x,ry);

    typedef typename Traits::ElementType::EntityPointer EP;
    EP ep(e);
    while(ep->level() != tf_level){
      assert(ep->hasFather());
      ep = ep->father();
    }
    typename TF::Traits::RangeType ty;
    typename Traits::DomainType tx = ep->geometry().local(e.geometry().global(x));
    tf.evaluate(*ep,tx,ty);
    
    ry -= ty;
    y = ry * ry;
  }

  //! get a reference to the GridView
  inline const typename Traits::GridViewType& getGridView () const
  {
    return rf.getGridView();
  }

private:
  const RF & rf;
  const TF & tf;
  const int tf_level;
  const int rf_level;
};

/**
   \brief This grid function computes the square of a given grid function.

   \tparam RF A grid function. May also be vector valued.
 */
template<typename RF>
class SquaredAdapter
  : public Dune::PDELab::TypeTree::LeafNode, Dune::PDELab::GridFunctionInterface<
  Dune::PDELab::GridFunctionTraits<
    typename RF::Traits::GridViewType,
    typename RF::Traits::RangeFieldType,
    1,
    Dune::FieldVector<typename RF::Traits::RangeFieldType,1>
    >,
  SquaredAdapter<RF>
  >
{
public:

  typedef Dune::PDELab::GridFunctionTraits<
  typename RF::Traits::GridViewType,
  typename RF::Traits::RangeFieldType,
  1,
  Dune::FieldVector<typename RF::Traits::RangeFieldType,1>
  > Traits;

  SquaredAdapter (const RF & rf_)
    : rf(rf_)
  {}

  // Evaluate
  inline void evaluate (const typename Traits::ElementType& e, 
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {  
    typename RF::Traits::RangeType ry;
    rf.evaluate(e,x,ry);
    y = ry * ry;
  }

  //! get a reference to the GridView
  inline const typename Traits::GridViewType& getGridView () const
  {
    return rf.getGridView();
  }

private:
  const RF & rf;
};

#endif
