#ifndef  TESTING_UTILITY_HH
#define  TESTING_UTILITY_HH

#define BEGIN_BLOCK(grid) std::cout << "!BEGIN-BLOCK! " << #grid << std::endl;
#define END_BLOCK(grid) std::cout << "!END-BLOCK! " << #grid << std::endl;

#define TEST_OUTPUT(quantity, value) std::cout << "!T! " << quantity << ": " << value << std::endl;

#endif 
