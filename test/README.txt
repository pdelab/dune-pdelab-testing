# Created by Hanna Remmel
# This README-file contains basic information about this system test environment.
# 
# 1. Test execution
# -----------------
#
# Use script all_tests.sh to execute all defined tests. If you wish to exclude some test programs, you can do this by commenting the according lines out in all_test_programs.tst.
#
#
# 2. Add new test program and/or new test cases
# ---------------------------------------------
#
# Each test application source code is located under “src” in an own subfolder. The according test case definitions are located under “test” in a subfolder with the same name. This makes it easy to find the  correct test case definitions for a test application and vice versa. 
# In our system test environment, the output for the algorithm verification consists of a free text description of the mathematical quantity and the according value followed by a colon. Use macro TEST_OUTPUT to insert algortihm verification output to the test program.
#
# Use script init_test_app.py for adding new test programs and/or new test cases. See comments at the beginning of the sript.
# 
#
# 3. Different files and their meaning
# ------------------------------------
#
# The file structure has two main folders: “src” for the C++ source code of the test applications and “test” for the test case definitions, for the results and the shell, perl and python scripts for the test execution.
#
# src\<test program>
# - - - - - - - - - 
# Subfolders in src\ each include the source code for a test program. See section 2. for including new test programs. 
#
# src\utility\utility.hh
# - - - - - - - - - - -
# This file includes the definitions of macros for creating TEST_OUTPUT and defining blocks with BEGIN_BLOCK and END_BLOCK. The macros for defining blocks should be used together with the if-block HAVE_<GRID> so that the test evaluation can exclude reference values for grid-blocks that are not defined on the used computer. Otherwise the test will not pass.
#
# test\all_test_programs.tst
# - - - - - - - - - - - - - 
# Inlcudes the list of all test programs that should be executed. If you wish to exclude some test programs, you can do this by commenting the according lines out here.
#
# test\<test program>\all_tests.tst
# - - - - - - - - - - - - - - - - -
# The core of our test case definitions for a test program is the configuration file “all_tests.tst” for an overview of all test cases for this test program. The configuration file includes the links to the test case 
parameter configuration file with the file extension “ini” and to the algorithm verification reference 
file with the file extension “ref”.
# With the optional parameter "processes" you can specify the number of used processes for the test case. 
#
# test\<test program>\<test case>.ini
# - - - - - - - - - - - - - - - - - -
# This file consists of the test case parameter configuration. The used parameters depend on the parameter configuration in the test program source code. 
#
# test\<test program>\<test case>.ref
# - - - - - - - - - - - - - - - - - -
# This file includes the reference values for test programs algorithm verification output. Since numerical solutions are carried out using floating point arithmetics and thus include rounding errors, the system test environment cannot expect an exact value for each algorithm verification test value. The configuration file of the expected values for algorithm verification includes the possibility of defining a tolerance range for each of the values.
#
# test\<test program>\reference_vtu\
# - - - - - - - - - - - - - - - - - 
# This folder includes the vtu-files used for the scientific validation. On test execution, the system test environment checks for each vtu output file if there is a reference_vtu file with the same name. If yes, the script ascii_vtk_diff.py is used to validate the output file.
#
# test\init_test_app.py
# - - - - - - - - - - -
# To support the scientists in extending the test environment, this script initializes a new test application and test cases into the test environment. This script creates necessary configuration files and also initializes the algorithm verification and scientific validation reference files. All configuration files can still be modified manually by the scientists. For details see comments in init_test_app.py.
#
# test\all_tests.sh
# - - - - - - - - -
# Use this script to execute all test programs and test cases according to the configuration.
#
# test\ascii_vtk_diff.pl
# - - - - - - - - - - - 
# This script is used for the scientific validation (comparison for vtk files).
#
# test\test_app.py
# - - - - - - - - 
# Executes the test for one test case. For details see comments in test_app.py.
#
# test\test_apps.py
# - - - - - - - - -
# Reads the configuration file all_tests.tst and executes test_app.py for each test case.
#
# 4. test results
# ---------------
#
# The test run results are saved in a folder structure according to the date and time of the test run. For each test case, it includes the test case output “log” file and the graphical “vtu” output files. Besides the output for algorithm verification and scientific validation, the output for each test run includes information about the source control version of each included DUNE module and about local changes on the test executer’s computer on these modules (output file “svn_info.log”, see Figure 6). This helps to verify the source code version for each test run. The execution time and the success of failure for each test case is documented in the output file “testrun.log”.  
#
#
# 5. System test environment concept
# ----------------------------------
#
# The system test environment consists of an infrastructure including the scripts for test execution and a specific file structure. The scientists then include a set of test applications that each implement a specific problem including the used mathematical and numerical models into this infrastructure. Each of these test applications has a set of parameters. 
# A test case for a test application can be defined by selecting a value for each test application parameter. Additionally, a test case can be run sequentially or parallel on a specific number of processors. A test run means the execution of all test applications with all their test cases. 
# The testing performed by the system test environment can be separated into two main parts: algorithm verification and scientific validation. Each test case has reference files including the expected values for the test output. The reference files for algorithm verification and scientific validation are separated. 
#
# Algorithm verification
# - - - - - - - - - - - -
# For algorithm verification the scientists extend each test applications output in a way that, depending on the used mathematical and numerical model, it includes some significant mathematical quantities like grid convergence rate or count of iterations. If possible, the reference values for these mathematical quantities are determined analytically. If this is not possible, like it typically isn’t for scientific software, the scientists set up these values from a scientifically validated run of the test application. 
# A change in these expected values always indicates a change in the test applications behavior. In most cases this means that there is a defect in the DUNE framework. The other possibility is that the framework was changed in a way that a change in this specific test application was expected. In this case, the scientists can update the reference values for the test case. Such changes always have to be scientifically justified and carefully documented. 
#
# Scientific validation
# - - - - - - - - - - -
# The system test environment supports the scientific validation through comparison of the graphical simulation output files. The values in these output files are compared with according scientific validation reference files. Both, absolute and relative difference between the output file values and reference file values are tested. This way minor change in the values is automatically accepted. If there any bigger change in the values in these files, the test environment reports it. 
# Again, it is the responsibility of the scientists to decide whether changes in the output are due to a defect or due to a planned change in the framework. As an example, when the scientists slightly changed the way the mathematical model for the test application “instationary” was structured, the output looked visually the same, but the single output values were still slightly different. In this case the scientists replaced the scientific validation reference files for this test application accordingly.
#
