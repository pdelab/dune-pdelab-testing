#!/bin/bash

#
# Use this script to execute all test programs and test cases according to the configuration.
#

# create output folders
out_date=`date +%Y-%m-%d`
out_time=`date +%T`

mkdir results -p
cd results
mkdir $out_date -p
cd $out_date
mkdir $out_time -p
cd $out_time
output_folder=`pwd`
cd ../../../

echo "lists svn info and svn status for dune-pdelab-testing and all depending modules..." 2>&1 |tee -a $output_folder/testrun.log
current_folder=`pwd`
cd ../../
modules=$(./dune-common/bin/dunecontrol --module=dune-pdelab-testing print)
for module in $modules
do
    cd $module
    svn info >> $output_folder/svn_info.log 2>&1
    svn status -q -u >> $output_folder/svn_info.log 2>&1
    cd ..
done
cd $current_folder

start_time=$(date +%s.%N)
errors=0
first=1
# hier wird kein exec<all_test_programs.tst benutzt, weil ein paralleler Aufruf eines Testprogramms exec leert
names=`cat all_test_programs.tst` 
for line in $names
do
    if [ ! '#' = ${line:0:1} ]
    then
        if [ 1 -eq $first ]
        then
            cd $line && ../test_apps.py all_tests.tst $output_folder 2>&1 |tee -a $output_folder/testrun.log
            let errors+=${PIPESTATUS[0]}
            first=0
        else
            cd ../ && cd $line && ../test_apps.py all_tests.tst $output_folder 2>&1 |tee -a $output_folder/testrun.log
            let errors+=${PIPESTATUS[0]}
        fi
        echo "---------------------------------------------------------------------------------" 2>&1 |tee -a $output_folder/testrun.log
    fi
done

if [ 0 -eq $errors ]
then
    echo "All tests succeeded!" 2>&1 |tee -a $output_folder/testrun.log
else
    echo "Not all tests succeeded!" 2>&1 |tee -a $output_folder/testrun.log
    echo "Unsuccessfull tests:" 2>&1 |tee -a $output_folder/testrun.log
    grep "TEST_ERROR:" $output_folder/testrun.log |tee -a $output_folder/testrun.log
fi

end_time=$(date +%s.%N)
eclapse_time=$(echo "$end_time - $start_time"|bc)
echo "Total time for test execution: $eclapse_time seconds." 2>&1 |tee -a $output_folder/testrun.log

echo "Log files 'testrun.log' and 'svn_info.log' for test execution can be found here: $output_folder" 2>&1 |tee -a $output_folder/testrun.log

