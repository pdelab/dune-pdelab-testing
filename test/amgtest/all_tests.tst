# This file includes the configuration for each test program call
# init_test_app.py creates one example test call. See other all_tests.tst files for further examples

[amgtest_seq]
path : ../../src/amgtest/
test_command : amgtest
test_command_args : amgtest_seq.ini
reference_file : amgtest_seq.ref

[amgtest_par1]
path : ../../src/amgtest/
test_command : amgtest
test_command_args : amgtest_par1.ini
reference_file : amgtest_par1.ref
processes : 1

[amgtest_par2]
path : ../../src/amgtest/
test_command : amgtest
test_command_args : amgtest_par2.ini
reference_file : amgtest_par2.ref
processes : 2

[amgtest_par4]
path : ../../src/amgtest/
test_command : amgtest
test_command_args : amgtest_par4.ini
reference_file : amgtest_par4.ref
processes : 4
