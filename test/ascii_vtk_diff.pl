#!/usr/bin/perl
#------------------------------------------------------------------------
#
# Filename: ascii_vtk_diff.pl
# Perlscript used to compare two ascii text files (e.g. vtk output).
#
#------------------------------------------------------------------------
my $left = shift;
my $right = shift;
my $epsilon_abs = shift;
my $epsilon_rel = shift;

$left = "-" unless defined $left;
$right = "-" unless defined $right;
$epsilon_abs = 0 unless defined $epsilon_abs;
$epsilon_rel = 0 unless defined $epsilon_rel;

if($left eq "-" and $right eq "-") {
    print STDERR "Compare two files allowing slight deviations in numbers\n";
    print STDERR "usage: fuzzy_diff <file1> <file2> <epsilon_abs> <epsilon_rel>\n";
    exit 1;
}

open LEFT, $left;
open RIGHT, $right;

my $num = qr{[+-]?(?:[0-9]+(?:\.[0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?};

my $counter = 0; # counting lines that are different

for(my $i = 0;; ++$i) {
    my $lline = <LEFT>;
    my $rline = <RIGHT>;
    if(!defined $lline and !defined $rline) { last }
    if(!defined $lline and defined $rline) {
		$counter++;
        print "!VTK! $left is shorter than $right\n";
        last;
    }
    if(defined $lline and !defined $rline) {
		$counter++;
        print "!VTK! $left is longer than $right\n";
        last;
    }
    chomp $lline;
    chomp $rline;
    my @left = map { s/[ \t]+/ /; s/^ $//; $_ } split /($num)/, $lline;
    my @right = map { s/[ \t]+/ /; s/^ $//; $_ } split /($num)/, $rline;
    if(@left == @right) {
        for(my $j = 0; $j < @left; ++$j) {
            if($j%2 == 0
               ? $left[$j] ne $right[$j]
               : ((abs($left[$j] - $right[$j]) > $epsilon_abs) && (abs($left[$j] - $right[$j]) > $epsilon_rel * abs($left[$j] + $right[$j]))))
            {
				$counter++;
                print "!VTK! Difference in $left:\n!VTK! $i:-$lline\n!VTK! $i:+$rline\n";
                last;
            }
        }
    } else {
		$counter++;
        print "!VTK! Difference in $left:\n!VTK! $i:-$lline\n!VTK! $i:+$rline\n";
    }
}

if( $counter > 0 )
{
	print "!VTK! Summary: fuzzy_diff found $counter different lines between $left and $right.\n"
}
else
{
	print "Summary: fuzzy_diff : '$left' and '$right' are equal.\n"
}

