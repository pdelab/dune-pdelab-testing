#!/bin/bash

#
# Executes all test programs and test cases according to the configuration.
#
mkdir tmp_output -p
cd tmp_output
output_folder=`pwd`
cd ../

first=1
# hier wird kein exec<all_test_programs.tst benutzt, weil ein paralleler Aufruf eines Testprogramms exec leert
names=`cat all_test_programs.tst` 
for line in $names
do
    if [ ! '#' = ${line:0:1} ]
    then
        if [ 1 -eq $first ]
        then
            cd $line && ../autobuild_test_apps.py all_tests.tst $output_folder
            first=0
        else
            cd ../ && cd $line && ../autobuild_test_apps.py all_tests.tst $output_folder
        fi
    fi
done

# delete tmp_output
cd ../
rm -r tmp_output
