[diffusionCube_dim2_level8_FEM_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim2_level8_FEM_k1.ref

[diffusionCube_dim2_level7_FEM_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim2_level7_FEM_k2.ref

[diffusionCube_dim2_level6_SIPG_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args :
reference_file : diffusionCube_dim2_level6_SIPG_k1.ref

[diffusionCube_dim2_level6_SIPG_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args :
reference_file : diffusionCube_dim2_level6_SIPG_k2.ref

[diffusionCube_dim2_level6_SIPG_k3]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim2_level6_SIPG_k3.ref

[diffusionCube_dim3_level5_FEM_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim3_level5_FEM_k1.ref

[diffusionCube_dim3_level4_FEM_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim3_level4_FEM_k2.ref

[diffusionCube_dim3_level5_SIPG_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim3_level5_SIPG_k1.ref

[diffusionCube_dim3_level4_SIPG_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim3_level4_SIPG_k2.ref

[diffusionCube_dim3_level3_SIPG_k3]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim3_level3_SIPG_k3.ref

[diffusionCube_dim3_level2_SIPG_k4]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionCube_dim3_level2_SIPG_k4.ref

[diffusionSimplex_dim2_level8_FEM_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim2_level8_FEM_k1.ref

[diffusionSimplex_dim2_level5_FEM_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim2_level5_FEM_k2.ref

[diffusionSimplex_dim2_level6_FEM_k3]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim2_level6_FEM_k3.ref

[diffusionSimplex_dim2_level4_SIPG_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim2_level4_SIPG_k1.ref

[diffusionSimplex_dim2_level6_SIPG_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim2_level6_SIPG_k2.ref

[diffusionSimplex_dim2_level6_SIPG_k3]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim2_level6_SIPG_k3.ref

[diffusionSimplex_dim3_level4_FEM_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim3_level4_FEM_k1.ref

[diffusionSimplex_dim3_level4_FEM_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim3_level4_FEM_k2.ref

[diffusionSimplex_dim3_level3_FEM_k3]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim3_level3_FEM_k3.ref

[diffusionSimplex_dim3_level2_FEM_k4]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim3_level2_FEM_k4.ref

[diffusionSimplex_dim3_level4_SIPG_k1]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim3_level4_SIPG_k1.ref

[diffusionSimplex_dim3_level3_SIPG_k2]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args : 
reference_file : diffusionSimplex_dim3_level3_SIPG_k2.ref

[diffusionSimplex_dim3_level3_SIPG_k3]
path : ../../src/diffusion/ 
test_command : diffusion
test_command_args :
reference_file : diffusionSimplex_dim3_level3_SIPG_k3.ref

