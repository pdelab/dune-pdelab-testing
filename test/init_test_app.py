#!/usr/bin/python

import subprocess, os, sys, re, test_app, shutil, ConfigParser

# This script can be used to insert a new test program and/or test casess to the system test environment
#
# 1. Insert test program in src
# -----------------------------
# A new test program should be placed under test/src in an own subfolder <test program>.
# For this script: this folder should be named equaly to the executable.
# If the test program has any parameter, they should be read using a configuration file
# (see the use of Dune::ConfigParser in other test programs, eg. diffusion).
# 
# 2. Insert test output for the mathematical magnitudes you want to test
# ----------------------------------------------------------------------
# In test program, the output should include those values, that you want to test. 
# The format for these output is:
# !T! <name of the value>: <value>
# Please use macro TEST_OUTPUT. For examples see other existing test programs.
#
# s. Initialize test program configuration
# ----------------------------------------
# Call this script in test folder to initialize the test program configuration
# Use test program name
# 
# The steps done in this script:
# 1. create subfolder for test program under test and sub folder "reference_vtu"
# 2. create all_tests.tst with an example test case and associated ref and ini -files
# 3. extend all_test_programs.tst
# 4. try to execute the test
#   4a. create ref file out of the output with default tolerance
#   4b. copy created vtu files in reference_vtu 

parameter_list = sys.argv

param_len = len(parameter_list)
if param_len < 2 or param_len > 4 :
    print "Syntax: ./init_test_app.py <test_program (, force_ref=0, force_vtu=0)>"
    exit(1)

test_program = parameter_list[1]
force_ref=0
if param_len > 2 :
    force_ref = int(parameter_list[2])
force_vtu=0
if param_len > 3 :
    force_vtu = int(parameter_list[3])
# print "force_ref: " + str(force_ref) + ", force_vtu: " + str(force_vtu)

# TODO: Check that we are in test folder
current_dir = os.getcwd()

# 1. create subfolder for test program under test and sub folder "reference_vtu"
print "Creating subfolder for " + test_program + "...",
already_exist=True
if not os.path.isdir(test_program) :
    os.mkdir(test_program)
    already_exist=False
os.chdir(test_program)
ref_vtu_dir = 'reference_vtu'
if not os.path.isdir(ref_vtu_dir) :
    os.mkdir(ref_vtu_dir)
    already_exist=False
if already_exist :
    print "already exist."
else :
    print "done."

# 2. create all_tests.tst with an example test case and associated ref and ini -files
path='../../src/' + test_program + '/'
test_command_args = test_program + '1.ini'
reference_file = test_program + '1'

print "Creating all_tests.tst for " + test_program + "...",
if not os.path.isfile('all_tests.tst') :
    all_tests = open('all_tests.tst','w')
    all_tests.write('# This file includes the configuration for each test program call\n')
    all_tests.write('# init_test_app.py creates one example test call. See other all_tests.tst files for further examples\n\n')
    all_tests.write('[' + test_program + '1]\n')
    all_tests.write('path : ' + path + '\n')
    all_tests.write('test_command : ' + test_program + '\n')
    all_tests.write('test_command_args : ' + test_command_args + '\n')
    all_tests.write('reference_file : ' + reference_file + '\n')
    all_tests.close()
    print "done."
else :
    print "already exists."

# 2a. Read file all_tests.tst and for each test configuration section create ref and ini -files
config = ConfigParser.RawConfigParser()
config.read('all_tests.tst')
sections = config.sections()
for sec in sections :
    at_path           = config.get(sec,'path')
    at_test_command   = config.get(sec,'test_command')
    ini_file_name     = config.get(sec,'test_command_args')
    at_reference_file = config.get(sec,'reference_file')
    at_processes      = ""
    if config.has_option(sec,'processes'):
        at_processes = config.get(sec,'processes')

#    ini_file_name = test_program + '1.ini'
    print "Creating " + ini_file_name + " for " + at_test_command + "...",
    if not os.path.isfile(ini_file_name) :
        ini_file = open(ini_file_name,'w')
        ini_file.write('# This ini-file includes the parameter values for a test case\n')
        ini_file.write('# [domain]\n')
        ini_file.write('# mesh = simplex\n')
        ini_file.write('# dim = 2\n')
        ini_file.write('# maxlevel = 6\n')
        ini_file.write('# method = SIPG\n')
        ini_file.write('# degree = 1\n')
        ini_file.close()
        print "done."
    else :
        print "already exists."

    if force_ref==1 :
        if os.path.isfile(at_reference_file) :
            os.remove(at_reference_file)
    print "Creating " + at_reference_file + " for " + at_test_command + "...",
    if not os.path.isfile(at_reference_file) :
        ref_file = open(at_reference_file,'w')
        ref_file.write('# This ref-file includes the test values and their tolerance\n')
        ref_file.write('# Format: \n')
        ref_file.write('# !T! <name of the value>: <value> +- <tolerance>\n')
        ref_file.close()
        print "done."
    else :
        print "already exists."

    # 3. extend all_test_programs.tst
    print "Extending all_test_programs.tst for " + at_test_command + "...",
    os.chdir('../')
    tp_regex = re.compile(at_test_command)
    found=False
    all_test_programs = open('all_test_programs.tst','r')
    for line in all_test_programs :
        data = tp_regex.search(line)
        if data is not None:
            found=True
            break
    all_test_programs.close()
    if not found:
        all_test_programs = open('all_test_programs.tst','a')
        all_test_programs.write(at_test_command + '\n')
        all_test_programs.close()
        print "done."
    else :
        print "already exists."
        
    # 4. try to execute the test
    print "Trying to execute " + at_test_command + "...",
    os.chdir(at_test_command)
    output_folder=at_test_command + '_testoutput'
    if not os.path.isdir(output_folder) :
        os.mkdir(output_folder)
    if not True : #os.path.isdir(at_path) :
        print "\nNOTE: Could not find " + at_test_command + " in " + at_path
        print "      Make sure that the testprogram exists."
    else :
        success=True
        try :
            test_app.apply(at_path,at_test_command,ini_file_name,at_reference_file,at_processes,output_folder)
        except:
            success=False
            print "! NOTE: Could not execute " + at_test_command + " successfully."
            print "! If the test program needs any parameters, please edit ini-file " + at_test_command + '/' + ini_file_name + "."
            print "! After making the required corrections, you can run this script again to fill in the ref file and to create reference vtu files."
        else :
            # Check if ref file is still empty
            print "Fill in ref file according to test program output...",
            ref_regex =re.compile(r"^\s*!T!")
            ref_file_r = open(at_reference_file,'r')
            ref_filled=False
            for line in ref_file_r :
                data = ref_regex.search(line)
                if data is not None:
                    ref_filled=True

            posRef = at_reference_file.find('.ref')
            at_reference_folder = at_reference_file[:posRef]
            os.chdir(output_folder + '/' + at_reference_folder)
            #   4a. create ref file out of the output with default tolerance
            if not ref_filled :
                data_regex = re.compile(r"^\s*!T!\s*([\S\s]+):\s*([\d\.eE+-]*)\s*#*")
                value_regex = re.compile(r"\d\.(\d*)[eE]-(\d*)")
                block_begin_regex = re.compile(r"^\s*!BEGIN-BLOCK!\s*([\S\s]+)\s*#*")
                block_end_regex = re.compile(r"^\s*!END-BLOCK!\s*([\S\s]+)\s*#*")
                output_file = open(at_reference_folder + '.log',"r")
                ref_file = open('../../' + at_reference_file,"a")
                for line in output_file :
                    data = data_regex.search(line)
                    if data is not None:
                        value = data.group(2)
                        value_data = value_regex.search(value)
                        if value_data is not None:
                            count1 = len(str(value_data.group(1)))
                            count2 = int(value_data.group(2))
                            ref_file.write(line.strip() + " +- 1e-" + str(count1 + count2 - 1) + "\n")
                        else :
                            ref_file.write(line.strip() + " +- 0\n")
                    block_begin = block_begin_regex.search(line)
                    block_end = block_end_regex.search(line)
                    if block_begin is not None or block_end is not None:
                        ref_file.write(line.strip() + "\n")
                print "done."
            else :
                print "already filled in. If you wish to override existing ref-file: use parameter force_ref."

            #   4b. copy created vtu files in reference_vtu
            print "Copying created vtu files to reference_vtu...",
            dir_entries = os.listdir('.')
            pattern = re.compile(r"\.vtu$")
            moved=False
            for entry in dir_entries :
                result = pattern.search(entry)
                if (result is not None) :
                    vtu_entry='../../' + ref_vtu_dir + '/'+ entry
                    if (os.path.isfile(vtu_entry) and force_vtu==1) :
                        os.remove(vtu_entry)
                    if not os.path.isfile(vtu_entry) :
                        shutil.move(entry,'../../' + ref_vtu_dir)
                        moved=True
            if moved :
                print "done."
            else :
                print "nothing to do. If you wish to override existing vtu-files, use     parameter force_vtu."
   
            os.chdir('../../')
    
    if success :    
        print "! Test program is now ready configured and will be executed when all_tests.sh is called."
        print "! Default error tolerance defined in ref-file(s) is +-0. If other error tolerance is suitable, please adjust ref-file(s)."
        print "! Example:   !T! dG-Level=0 L2ERROR: 1.1119996E-02 +- 1e-8"
        print "! If test program requires any parameters and you wish to test several combinations of these parameters, you need to:"
        print "! 1. insert a new section in " + test_program + "/all_tests.tst"
        print "! 2. call this script for the test program to expand the configuration"

# delete output_folder
shutil.rmtree(output_folder)
exit(0)
