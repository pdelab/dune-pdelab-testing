[instationarytest_SIPG_level1]
path : ../../src/instationarytest/ 
test_command : instationarytest
test_command_args : instationarytest_SIPG_level1.ini
reference_file : instationarytest_SIPG_level1.ref

[instationarytest_SIPG_level2]
path : ../../src/instationarytest/ 
test_command : instationarytest
test_command_args : instationarytest_SIPG_level2.ini
reference_file : instationarytest_SIPG_level2.ref

[instationarytest_SIPG_level3]
path : ../../src/instationarytest/ 
test_command : instationarytest
test_command_args : instationarytest_SIPG_level3.ini
reference_file : instationarytest_SIPG_level3.ref

[instationarytest_FEM_level1]
path : ../../src/instationarytest/ 
test_command : instationarytest
test_command_args : instationarytest_FEM_level1.ini
reference_file : instationarytest_FEM_level1.ref

[instationarytest_FEM_level2]
path : ../../src/instationarytest/ 
test_command : instationarytest
test_command_args : instationarytest_FEM_level2.ini
reference_file : instationarytest_FEM_level2.ref

[instationarytest_FEM_level3]
path : ../../src/instationarytest/ 
test_command : instationarytest
test_command_args : instationarytest_FEM_level3.ini
reference_file : instationarytest_FEM_level3.ref
