[nonlineardiffusion_level1]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level1.ini
reference_file : nonlineardiffusion_level1.ref

[nonlineardiffusion_level2]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level2.ini
reference_file : nonlineardiffusion_level2.ref

[nonlineardiffusion_level4]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level4.ini
reference_file : nonlineardiffusion_level4.ref

[nonlineardiffusion_level1_par1]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level1_par1.ini
reference_file : nonlineardiffusion_level1_par1.ref
processes: 1

[nonlineardiffusion_level2_par1]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level2_par1.ini
reference_file : nonlineardiffusion_level2_par1.ref
processes: 1

[nonlineardiffusion_level1_par2]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level1_par2.ini
reference_file : nonlineardiffusion_level1_par2.ref
processes: 2

[nonlineardiffusion_level2_par2]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level2_par2.ini
reference_file : nonlineardiffusion_level2_par2.ref
processes: 2

[nonlineardiffusion_level2_par4]
path : ../../src/nonlineardiffusion/ 
test_command : nonlineardiffusion
test_command_args : nonlineardiffusion_level2_par4.ini
reference_file : nonlineardiffusion_level2_par4.ref
processes: 4
