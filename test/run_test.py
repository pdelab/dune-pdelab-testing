#!/usr/bin/python

import subprocess, os, sys, ConfigParser, time, test_app, shutil

def run_test(test_program, test_section):
    # this script is called from autobuild folder
    config_file = "../test/" + test_program + "/all_tests.tst"
    config = ConfigParser.ConfigParser()
    try:
        config.read(config_file)
    except:
        print "Error while attempting to read configuration file " + parameter_list[1]
        return 1  

    if not config.has_section(test_section) :
        print "Section " + test_section + " does not exist." 
        return 1

    output_folder = "../"

    current_dir = os.getcwd()
    os.chdir("../test/" + test_program)

    all_success = True

    path = config.get(test_section,"path")
    test_command = config.get(test_section,"test_command")
    test_command_args = config.get(test_section,"test_command_args")
    reference_file = config.get(test_section,"reference_file")
    processes = ""
    if (config.has_option(test_section, "processes")) : 
        processes = config.get(test_section,"processes")
    start_time = time.time()
    try:
        all_success = test_app.apply(path,test_command,test_command_args,reference_file,processes,output_folder,show_summary_text=0)
    except:
        raise
    print "Total time for this section: " + str(time.time() - start_time) + " seconds"

    os.chdir("../")
    shutil.rmtree(test_section)
    
    if not all_success : 
        return 1
    else : 
        return 0

