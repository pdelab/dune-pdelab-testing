import re, subprocess, itertools, time, os, datetime, shutil

def apply(path, test_command, test_command_args, reference_file, processes, output_folder, show_summary_text=1):
    """ This class performs a test run of a given binary program with
    given command line arguments and compares the output with a
    reference output given in a reference text file. Only those output
    lines which start with a !T! are relevant for the test and all
    others will be ignored.

    path : The path to the test application.

    test_command : The executable command of the test application.

    test_command_args : The command line parameters to be passed to
    the test application.

    reference_file : The filename of the reference file. All reference
    files have to use the ending *.ref . Output files containing the
    whole output of the test application will be produced and will use
    the same name with ending *.out .

    """

    data_regex = re.compile(r"^\s*!T!\s*([\S\s]+):\s*([\d\.eE+-]*)\s*\+-\s*([\d\.eE+-]*)\s*#*")
    comment_regex = re.compile(r"^\s*#")
    empty_regex = re.compile(r"^\s*$")
    block_begin_regex = re.compile(r"^\s*!BEGIN-BLOCK!\s*([\S\s]+)\s*#*")
    block_end_regex = re.compile(r"^\s*!END-BLOCK!\s*([\S\s]+)\s*#*")

    # The reference data set
    ref_data = [] 
    file = open(reference_file,"r")
    for line in file :
        data = data_regex.search(line)
        if data is None:
            comments = comment_regex.search(line)
            emptys = empty_regex.search(line)
            block_begin = block_begin_regex.search(line)
            block_end = block_end_regex.search(line)
            if block_begin is not None:
                testset = {}
                groups = block_begin.groups()
                testset["key"] = "BEGIN-BLOCK-" + str(groups[0])
                testset["value"] = 0
                testset["tolerance"] = 0
                ref_data.append(testset)
            else:
                if block_end is not None:
                    testset = {}
                    groups = block_end.groups()
                    testset["key"] = "END-BLOCK-" + str(groups[0])
                    testset["value"] = 0
                    testset["tolerance"] = 0
                    ref_data.append(testset)
                else:
                    if comments is None and emptys is None:
                        print "\tERROR: Invalid test file " + reference_file
                        exit(1)
        else :
            testset = {}
            groups = data.groups()
            testset["key"] = str(groups[0])
            testset["value"] = float(groups[1])
            testset["tolerance"] = float(groups[2])
            ref_data.append(testset)

    file.close()

    posRef = reference_file.find('.ref')
    reference_folder = reference_file[:posRef]

    current_dir = os.getcwd()
    os.chdir(output_folder)
    if not os.path.isdir(reference_folder) :
        os.mkdir(reference_folder)
    os.chdir(current_dir)
    
    # Execute process
    out_filename = output_folder + '/' + reference_folder + '/' + reference_folder + '.log'
    print 'Running test ' + test_command + ' ' + test_command_args

    start_time = time.time()

    path_complete = path
    if (processes != "") :
        path_complete = 'mpirun -np ' + processes + ' ' + path_complete
    
    try:
        subprocess.check_call(path_complete + '/' + test_command + ' ' + test_command_args + ' > ' + out_filename,shell=True)
    except:
        print "TEST_ERROR: Test program " + test_command + ' ' + test_command_args + " execution failed!"
        raise

    print "\tRun-time: " + str(time.time() - start_time) + " seconds"

    # compare every vtu-file in result folder with reference vtu-file (if exists)
    print ""
    print "Comparing VTK output files..."
    ref_vtu_dir = 'reference_vtu'
    if os.path.isdir(ref_vtu_dir) :
        dir_entries = os.listdir('.')
        refdir_entries = os.listdir(ref_vtu_dir)
        pattern = re.compile(r"\.vtu$")
        pattern_pvtu = re.compile(r"\.pvtu$")
        pattern_log = re.compile(r"\.log$")
        for entry in dir_entries :
            result = pattern.search(entry)
            result_pvtu = pattern_pvtu.search(entry)
            result_log = pattern_log.search(entry)
            if result is not None:
                # search for a file with same name in folder reference_vtu
                for ref_entry in refdir_entries :
                    if ref_entry == entry :
                        # if found: compare with script, output in .out -file
                        subprocess.check_call("../ascii_vtk_diff.pl" + ' ' + entry + ' ' + ref_vtu_dir + '/' + ref_entry + ' 1e-13 1e-10' + ' >> ' + out_filename,shell=True)
                shutil.move(entry,output_folder + '/' + reference_folder)
            else :
                if (result_pvtu is not None) :
                    os.remove(entry)
                else : 
                    if (result_log is not None) :
                        os.remove(entry)

    data_regex = re.compile(r"^\s*!T!\s*([\S\s]+):\s*([\d\.eE+-]*)\s*#*")
    vtk_regex = re.compile(r"^\s*!VTK!")

    # The test data set
    test_data = [] 
    comp_failed = False
    file = open(out_filename,"r")
    for line in file :
        data = data_regex.search(line)
        if data is not None:
            testset = {}
            groups = data.groups()
            testset["key"] = str(groups[0])
            testset["value"] = float(groups[1])
            test_data.append(testset)
        block_begin = block_begin_regex.search(line)        
        if block_begin is not None:
            testset = {}
            groups = block_begin.groups()
            testset["key"] = "BEGIN-BLOCK-" + str(groups[0])
            testset["value"] = 0
            testset["tolerance"] = 0
            test_data.append(testset)
        block_end = block_end_regex.search(line)
        if block_end is not None:
            testset = {}
            groups = block_end.groups()
            testset["key"] = "END-BLOCK-" + str(groups[0])
            testset["value"] = 0
            testset["tolerance"] = 0
            test_data.append(testset)
        vtk = vtk_regex.search(line)
        if vtk is not None:
            comp_failed = True
            print "\t ERROR: " + line

    file.close()

    if not comp_failed:
        print "\t o.k. All VTK outputs unchanged."

    # Before we check the test_data against the ref_data, we will remove the blocks in ref_data that are not included in test_data
    # We also remove all Block-statements
    i = 0
    while i < len(test_data):
        if i == len(ref_data):
            print "\tUnexpected combination of test output and reference file! " + \
            "Test is bound to fail!"
            break
#        print "i " + str(i)
#        print "test_data[i][key] " + test_data[i]["key"]
#        print "ref_data[i][key] " + ref_data[i]["key"]
        test_data_key = test_data[i]["key"]
        ref_data_key = ref_data[i]["key"]
        if test_data_key != ref_data_key:
#            print "ref_data_key " + ref_data_key
#            print "ref_data_key[0:12] " + ref_data_key[0:12]
            if ref_data_key[0:12] == "BEGIN-BLOCK-":
                # remove entries in ref_data until the end of the block
#                print "ref_data_key[0:10] " + ref_data_key[0:9]
                while ref_data_key[0:10] != "END-BLOCK-":
                    ref_data.pop(i)
                    ref_data_key = ref_data[i]["key"]
#                    print "in delete ref_data_key " + ref_data_key
#                    print "in delete ref_data_key[0:10] " + ref_data_key[0:10]
                # remove the end of the block
#                print "out of while"
                ref_data.pop(i)
            else:
                i = i+1
        else:
            # remove block-statements, since the should not be tested for correct value in the next step
            if test_data_key[0:12] == "BEGIN-BLOCK-" or test_data_key[0:10] == "END-BLOCK-":
                test_data.pop(i)
                ref_data.pop(i)
            else:
                i = i+1

    # remove block-statements in ref_data (needed if ref_data has more items that test_data)
    i = 0
    while i < len(ref_data):
        ref_data_key = ref_data[i]["key"]
        if ref_data_key[0:12] == "BEGIN-BLOCK-" or ref_data_key[0:10] == "END-BLOCK-":
            ref_data.pop(i)
        else:
            i = i+1   
            
    # test output
    print ""
    print "Comparing output with tagged values from reference solutions..."
    if len(test_data) != len(ref_data) :
        print "\tNotice: Test has different length from reference data! " + \
            "Test is bound to fail!"

    zero_set = {"key" : "", "value" : 0.0, "tolerance" : 0.0}

    comp_data = itertools.izip_longest(test_data,ref_data,fillvalue=zero_set)
    counter = -1
    for checkset in comp_data :
        counter=counter+1
        print "\tInspecting test set " + str(counter) + " with key \""+ checkset[1]["key"] + "\""
        if checkset[0]["key"] != checkset[1]["key"] :
            comp_failed = True
            print "\t ERROR: Keys " + checkset[0]["key"] + " - " + checkset[1]["key"] + \
                " do not agree."

        if abs(checkset[0]["value"] - checkset[1]["value"]) > checkset[1]["tolerance"] :
            comp_failed = True
            print "\t ERROR: Values " + str(checkset[0]["value"]) + " - " + str(checkset[1]["value"]) + \
                " do not agree."
        else :
            print "\t o.k."
            
    
    summary_text = "\nFor more details, see the test output file '" + out_filename + "' in the directory \n" + output_folder + "\n"

    if comp_failed : 
        print "TEST_ERROR: Test " + test_command + ' ' + test_command_args + " failed!"
        if show_summary_text==1:
            print summary_text
        return False

    print "OK: Test succeeded!"
    if show_summary_text==1:
        print summary_text
    return True


