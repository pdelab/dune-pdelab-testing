#!/usr/bin/python

#
# Reads the configuration file all_tests.tst and executes test_app.py for each test case.
#

import subprocess, os, sys, ConfigParser, time, test_app

parameter_list = sys.argv

if(len(parameter_list) != 3):
    print "Syntax: ./test_apps.py <test_app ini_file, result_folder>"
    exit(1)

config = ConfigParser.ConfigParser()
try:
    config.read(parameter_list[1])
except:
    print "Error while attempting to read configuration file " + parameter_list[1]
    exit(1)

output_folder = parameter_list[2]
if not os.path.isdir(output_folder) :
    print "Output folder " + output_folder + " does not exist." 
    exit(1)    

sections = config.sections()

all_success = True
for section in sections:
    path = config.get(section,"path")
    test_command = config.get(section,"test_command")
    test_command_args = config.get(section,"test_command_args")
    reference_file = config.get(section,"reference_file")
    processes = ""
    if (config.has_option(section, "processes")) : 
        processes = config.get(section,"processes")
    start_time = time.time()
    try:
        all_success = test_app.apply(path,test_command,test_command_args,reference_file,processes,output_folder) and all_success
    except:
        raise
    print "Total time for this section: " + str(time.time() - start_time) + " seconds"
    
if not all_success : 
    exit(1)
else : 
    exit(0)

    
        
